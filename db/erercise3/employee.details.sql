SELECT university.university_name
      ,college.name AS College_name
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.dept_name
      ,employee.name as employee_name
      ,designation.name as designation_name
      ,designation.rank as employee_rank
  FROM university_details.employee employee
 INNER JOIN university_details.designation
    ON employee.desig_id = designation.id
 INNER JOIN university_details.college_department
    ON employee.cdept_id = college_department.cdept_id
 INNER JOIN university_details.college
    ON college_department.college_id = college.id
 INNER JOIN university_details.department
    ON college_department.udept_code = department.dept_code
 INNER JOIN university_details.university
    ON college.univ_code = university.univ_code
 WHERE college.univ_code = (SELECT university.univ_code
                                 FROM university_details.university university
							    WHERE university.university_name = 'Anna university')
 ORDER BY college.name ASC
         ,designation.rank ASC