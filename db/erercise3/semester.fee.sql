INSERT INTO semester_fee(
       cdept_id
      ,stud_id
      ,semester
      ,amount
      ,paid_year
      ,paid_status)
SELECT cdept_id
      ,id AS stud_id
      ,6 AS semester
      ,100000 AS amount
      ,NULL AS paid_year
      ,'Unpaid' AS paid_status
  FROM student
  ORDER BY roll_number ASC;