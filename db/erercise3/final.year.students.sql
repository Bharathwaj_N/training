SELECT student.roll_number AS Roll_Number
      ,student.name AS Student_Name
      ,student.gender AS Gender
      ,student.dob AS DOB
      ,student.email AS Email
      ,student.phone AS Phone
      ,student.address AS Address
      ,student.academic_year AS Academic_Year
      ,college.name AS College_Name
      ,college.city AS City
      ,department.dept_name AS Department_Name
  FROM university_details.student student
 INNER JOIN university_details.college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN university_details.department
    ON department.dept_code = college_department.udept_code
 INNER JOIN university_details.college
    ON college.id = college_department.college_id
 INNER JOIN university_details.university
    ON college.univ_code = university.univ_code
 WHERE university.university_name IN ('Anna University', 'Harvard University', 'SRM University')
   AND college.city IN ('Coimbatore')
   AND student.academic_year = '2019'