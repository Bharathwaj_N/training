SELECT student.id
	  ,student.roll_number
      ,student.name
      ,student.gender
      ,college.code
      ,college.name
      ,semester_result.grade
      ,semester_result.credits AS GPA
  FROM university_details.university
      ,university_details.college
      ,university_details.student
      ,university_details.semester_result
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id
   AND semester_result.stud_id = student.id
   AND semester_result.credits>8;
SELECT student.id
	  ,student.roll_number
      ,student.name
      ,student.gender
      ,college.code
      ,college.name
      ,semester_result.grade
      ,semester_result.credits AS GPA
  FROM university_details.university
      ,university_details.college
      ,university_details.student
      ,university_details.semester_result
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id
   AND semester_result.stud_id = student.id
   AND semester_result.credits>5;