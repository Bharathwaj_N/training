SELECT university.university_name
      ,student.id AS ID
      ,student.roll_number AS Roll_Number
      ,student.name as student_name
      ,student.gender AS Gender
      ,student.dob AS DOB
      ,student.email AS Mail_Id
      ,student.address AS Address
      ,college.name AS College_Name
      ,semester_result.semester AS Semester
      ,semester_result.grade AS Grade
      ,semester_result.credits AS Credits
  FROM university_details.student student
 INNER JOIN university_details.college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN university_details.college college
    ON college.id = college_department.college_id
 INNER JOIN university_details.university university
    ON university.univ_code = college.univ_code
 INNER JOIN university_details.syllabus
    ON syllabus.cdept_id = college_department.cdept_id
 INNER JOIN university_details.semester_result semester_result
    ON semester_result.syllabus_id = syllabus.id
   AND semester_result.stud_id = student.id
 ORDER BY college.name ASC
		 ,semester_result.semester ASC
