SELECT t.code
      ,t.college_name
      ,univ.university_name
      ,t.city
      ,t.state
      ,t.year_opened
      ,department.dept_name
      ,t.hod_name
  FROM (SELECT clg.code
              ,clg.name AS college_name
              ,clg.univ_code
              ,clg.city
              ,clg.year_opened
              ,clg.state
              ,emp.name AS hod_Name
              ,emp.cdept_id
          FROM employee AS emp
               INNER JOIN college AS clg
               ON emp.college_id = clg.id
               INNER JOIN designation 
               ON emp.desig_id = designation.id
         WHERE designation.name = 'Head of Department') AS t
         INNER JOIN university AS univ
                 ON t.univ_code = univ.univ_code
         INNER JOIN college_department AS c_dept
                 ON c_dept.cdept_id = t.cdept_id
         INNER JOIN department
                 ON c_dept.udept_code = department.dept_code
  WHERE department.dept_name IN ('Computer Science Engineering', 'Information Technology');