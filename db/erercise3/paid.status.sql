UPDATE university_details.semester_fee
SET paid_status = 'Paid'
   ,paid_year = '2020'
WHERE stud_id = (SELECT student.id
                     FROM university_details.student student
                    WHERE student.roll_number = '117ce001');
                    
UPDATE semester_fee
SET paid_status = 'Paid'
   ,paid_year = 2017
WHERE stud_id IN (SELECT student.id
                     FROM university_details.student student
                    WHERE roll_number IN ('18EE062'
                                           ,'19EE039'
                                           ,'118ce003'
                                           ,'18CS032'
                                           ,'117ce002'
                                           ,'118ce003'
										   ,'117ce009'
                                           ,'118ce010'
                                           ,'118ce011'
                                           ,'117ce012'
                                           ,'117ce013'
                                           ,'117ce054'
                                           ,'117ce026'
										   ,'117cs001'
										   ,'117cs003'
                                           ,'118cs004'
                                           ,'117cs006'
                                           ,'118cs007'
										   ,'117cs009'
                                           ,'117cs017'
                                           ,'117cs025'
										   ,'117cs029'
                                           ,'118cs076'
										   ,'217ce001'
										   ,'217ce002'
									       ,'216ce001'
										   ,'217ce003'
										   ,'216ce002'
										   ,'217ce004'
										   ,'216ce003'
										   ,'217ce005'
										   ,'217ce006'
										   ,'217ce007'
										   ,'216cs001'
										   ,'216cs002'
										   ,'216cs003'
										   ,'216cs004'
										   ,'216cs005'
										   ,'217cs003'
										   ,'217cs004'
										   ,'217cs005'
										   ,'216ee001'
										   ,'216ec002'
										   ,'216ec003'
										   ,'216ec004'))