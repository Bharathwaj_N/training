SELECT A.first_name
      ,B.first_name
      ,A.area 
      ,B.area 
  FROM employee.employee_table A
      ,employee.employee_table B
 WHERE A.area = B.area
   AND A.department_number = B.department_number