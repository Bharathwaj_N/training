SELECT city
	  ,id
  FROM order.order_table ord
  LEFT JOIN order.customer_table customer
    ON ord.cust_code = customer.id
  LEFT JOIN order.agent_table agent
    ON agent.agent_code = customer.agent_code