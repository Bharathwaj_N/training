CREATE DATABASE product ;
CREATE TABLE product.product_details (
             product_id INT NOT NULL
            ,product_name VARCHAR(45) NOT NULL
            ,product_price INT NOT NULL
            ,number_of_product INT NOT NULL
            ,product_description VARCHAR(45) NOT NULL DEFAULT 'good'
            ,PRIMARY KEY (product_id)
			,INDEX product_price USING BTREE (product_price(5)) VISIBLE
            ,FOREIGN KEY (customer_id) REFERENCES product(customer_details) );

