CREATE DATABASE sports;
CREATE TABLE sports.sports_table (
             sport_name VARCHAR(45)
            ,number_of_player INT
            ,player_number INT
            ,number_of_match_played INT );

SHOW COLUMNS FROM sports.sports_table
