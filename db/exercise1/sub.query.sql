SELECT annual_salary
	  ,employee_id
      ,first_name
  FROM employee.employee_table
WHERE department_number >
( SELECT department_id
    FROM employee.department_table
   WHERE department_id = 1)