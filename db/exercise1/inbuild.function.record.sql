SELECT MIN(annual_salary)
  FROM employee.employee_table;
SELECT MAX(product_price)
  FROM product.product_details;
SELECT COUNT(department_number) 
	AS total_department
  FROM employee.employee_table;
SELECT SUM(product_price) 
    AS total_price
  FROM product.product_details;
SELECT dob, NOW()
    AS timing
  FROM employee.employee_table;
SELECT  product_name
  FROM product.product_details
 ORDER BY product_id ASC
 LIMIT 1;
SELECT  product_name
  FROM product.product_details
 ORDER BY product_id DESC
 LIMIT 1