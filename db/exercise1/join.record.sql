SELECT product_details.product_id
      ,product_details.product_name
      ,customer_details.product_id
      ,customer_details.customer_name
  FROM product.product_details
  LEFT JOIN product.customer_details
    ON product_details.product_id = customer_details.product_id;
SELECT *
  FROM product.product_details
 CROSS JOIN product.customer_details;
SELECT product_details.product_id
      ,product_details.product_name
      ,customer_details.product_id
      ,customer_details.customer_name
  FROM product.product_details
 RIGHT JOIN product.customer_details
    ON product_details.product_id = customer_details.product_id;
SELECT employee_table.department_number
	  ,department_table.department_id
  FROM employee.employee_table
 INNER JOIN employee.department_table
    ON employee_table.department_number = department_table.department_id;
SELECT product_details.product_id
      ,product_details.product_name
      ,customer_details.product_id
      ,customer_details.customer_name
  FROM product.product_details
  LEFT JOIN product.customer_details
    ON product_details.product_id = customer_details.product_id
 UNION ALL
 
SELECT product_details.product_id
      ,product_details.product_name
      ,customer_details.product_id
      ,customer_details.customer_name
  FROM product.product_details
 RIGHT JOIN product.customer_details
    ON product_details.product_id = customer_details.product_id;
