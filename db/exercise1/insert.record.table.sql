INSERT INTO product.customer_details (customer_id, customer_name) VALUES (1, 'Boopathy');
INSERT INTO product.customer_details (customer_id, customer_name) VALUES (2, 'Akash');
INSERT INTO product.customer_details (customer_id, customer_name) VALUES (3, 'Kishore');
INSERT INTO product.customer_details (customer_id, customer_name) VALUES (4, 'Sushanth');
INSERT INTO product.product_details (product_id, product_name, product_price, number_of_product, product_description) VALUES (151, 'Battery' , 150000, 5, 'Better');
INSERT INTO product.product_details (product_id, product_name, product_price, number_of_product, product_description) VALUES (1458, 'Study_lamp', 5000, 9, 'Better');
INSERT INTO product.product_details (product_id, product_name, product_price, number_of_product, product_description) VALUES (122, 'Marbles', 12000, 7, 'good');
INSERT INTO product.product_details (product_id, product_name, product_price, number_of_product, product_description) VALUES (1025, 'Vinegar', 200, 25, 'Excellent');
INSERT INTO product.product_details (product_id, product_name, product_price, number_of_product, product_description) VALUES (1124, 'MI_TV', 15000, 22, 'good');