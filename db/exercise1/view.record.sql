CREATE VIEW depart_tableview
    AS SELECT employee_id
             ,annual_salary
             ,department_id
  FROM employee.employee_table emp
      ,employee.department_table dept
 WHERE emp.employee_id = dept.department_id;
SELECT *
  FROM depart_tableview