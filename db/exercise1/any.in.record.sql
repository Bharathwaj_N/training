SELECT employee_id
  FROM employee.employee_table
 WHERE employee_id = ANY ( SELECT department_id FROM employee.department_table WHERE department_id = 1);
SELECT employee.employee_id
	  ,employee.first_name
      ,employee.surname
      ,employee.dob
      ,employee.date_of_joining
      ,employee.annual_salary
      ,employee.department_number
      ,employee.area  
  FROM employee.employee_table employee
 WHERE annual_salary IN (100000, 50000, 25000);
SELECT employee.employee_id
	  ,employee.first_name
      ,employee.surname
      ,employee.dob
      ,employee.date_of_joining
      ,employee.annual_salary
      ,employee.department_number
      ,employee.area 
  FROM employee.employee_table
 WHERE annual_salary NOT IN (100000, 50000,25000)