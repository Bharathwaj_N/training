SELECT product.product_id
	  ,product.product_name
      ,product.product_price
      ,product.number_of_product
      ,product.product_description
  FROM product.product_details product
 WHERE product_name = 'Marbles' AND number_of_product = 7;
SELECT employee_id
	  ,annual_salary
      ,department_number
  FROM employee.employee_table
 WHERE employee_id = 25 OR annual_salary = 100000;
SELECT department.department_id
      ,department.department_name
  FROM employee.department_table department
 WHERE NOT department_id = 6
