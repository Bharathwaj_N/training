UPDATE employee.employee_table
   SET annual_salary = annual_salary + (annual_salary * 0.1);
SELECT employee.employee_id
	  ,employee.first_name
      ,employee.surname
      ,employee.dob
      ,employee.date_of_joining
      ,employee.annual_salary
      ,employee.department_number
      ,employee.area
  FROM employee.employee_table employee