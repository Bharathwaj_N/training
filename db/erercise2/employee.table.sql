SELECT employee.first_name
      ,employee.surname
      ,department.department_name
  FROM employee.employee_table employee
      ,employee.department_table department
 WHERE employee.department_number = department.department_id