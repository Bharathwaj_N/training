SELECT department.department_name
      ,MIN(annual_salary) AS least_paid
      ,MAX(annual_salary) AS high_paid
  FROM employee.employee_table 
      ,employee.department_table department
 WHERE department_number = department_id
 GROUP BY department_id