package com.kpr.training.jdbc.service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@SuppressWarnings("unused")
public class ConnectionService {
    
    private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();
    private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;
    {
        Properties properties = new Properties();

        try {
            InputStream resourceAsStream =
                    ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");

            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
            }

            config.setJdbcUrl(properties.getProperty(Constant.URL));
            config.setUsername(properties.getProperty(Constant.USERNAME));
            config.setPassword(properties.getProperty(Constant.PASSWORD));
            config.setMaximumPoolSize(Constant.MAX_POOL_SIZE);
            config.setDriverClassName("com.mysql.cj.jdbc.Driver");
            config.setAutoCommit(false);
            ds = new HikariDataSource(config);
        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS, e);
        }
    }
    
    public static void init() {
        try {
            threadLocal.set(ds.getConnection());
        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS, e);
        } 
    }
    
    public static Connection get() {
        if (threadLocal.get() == null) {
            init();
        }
        
        return threadLocal.get();
    }
    
    public static void release() {
        
        try {
            threadLocal.get().close();
            threadLocal.remove();
        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS_TO_CLOSE, e);
        }
    }

    public static void commit() {
        
        try {
            threadLocal.get().commit();
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_COMMIT);
        }
    }
    
public static void commitAndRollback(boolean select) {
        
        try {
            
            if (select) {
                threadLocal.get().commit();
            } else {
                threadLocal.get().rollback();
            }
            
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_COMMIT);
        }
    }
}