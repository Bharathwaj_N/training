/*
Requirement:
	To perform the CRUD operation of the Address.
 
Entity:
	1.Address
    2.AddressService
    3.AppException
    4.ErrorCode
 
Function declaration:
	public long create(Address address) {}
    public Address read(long id) {}
    public ArrayList<Address> readAll() {}
    public void update(Address address) {}
    public void delete(long id) {}
    public ArrayList<Address> search(String street, String city, String postalCode) {}
    public Address readAddress(ResultSet result) {}
Jobs To Be Done:
    1. Create a Address.
    2. Read a record in the Address.
    3. Read all the record in the addresses.
    4. Update an Address.
    5. Delete an Address.
    6. Search the address which contains given data.
    7. Perform common operations for read and readAll.
*/

package com.kpr.training.jdbc.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;

public class AddressService {

    @SuppressWarnings("static-access")
    public long create(Address address) {

        long addressID = 0;
        int numberOfRowsAffected;

        if (address.getPostalCode() == 0) {
            throw new AppException(ErrorCode.POSTAL_CODE_ZERO);
        } else {

            try {

                PreparedStatement ps = null;
                ps = ThreadPool.get().prepareStatement(QueryStatement.CREATE_ADDRESS_QUERY,
                        ps.RETURN_GENERATED_KEYS);
                ps.setString(1, address.getStreet());
                ps.setString(2, address.getCity());
                ps.setInt(3, address.getPostalCode());
                numberOfRowsAffected = ps.executeUpdate();
                ResultSet autoGeneratedID = ps.getGeneratedKeys();
                
                while (autoGeneratedID.next()) {
                    addressID = autoGeneratedID.getLong("GENERATED_KEY");
                }
                
                if (numberOfRowsAffected == 0 || addressID == 0) {
                    throw new AppException(ErrorCode.ADDRESS_CREATION_FAILS);
                }
                ps.close();
            } catch (Exception e) {
                throw new AppException(ErrorCode.ADDRESS_CREATION_FAILS, e);
            }

        }

        return addressID;
    }

    public Address read(long id) {

        Address address = null;

        try (PreparedStatement ps = ThreadPool.get().prepareStatement(QueryStatement.READ_ADDRESS_QUERY)) {

            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();
            
            while (result.next()) {
                address = readAddress(result);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILS, e);
        }

        return address;
    }

    public ArrayList<Address> readAll() {

        Address address = null;
        ArrayList<Address> addresses = new ArrayList<>();

        try (PreparedStatement ps = ThreadPool.get().prepareStatement(QueryStatement.READALL_ADDRESS_QUERY)) {

            ResultSet result = ps.executeQuery();

            while (result.next()) {
                address = readAddress(result);
                addresses.add(address);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILS, e);
        }

        return addresses;
    }

    public void update(Address address) {

        int numberOfRowsAffected = 0;

        if (address.getPostalCode() == 0) {
            throw new AppException(ErrorCode.POSTAL_CODE_ZERO);
        } else {

            try (PreparedStatement ps = ThreadPool.get().prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY)) {
 
                ps.setString(1, address.getStreet());
                ps.setString(2, address.getCity());
                ps.setInt(3, address.getPostalCode());
                ps.setLong(4, address.getId());
                numberOfRowsAffected = ps.executeUpdate();

                if (numberOfRowsAffected == 0) {
                    throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILS);
                }
            } catch (Exception e) {
                throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILS, e);
            }
        }
    }

    public void delete(long id) {

        int numberOfRowsAffected = 0;

        try (PreparedStatement ps = ThreadPool.get().prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY)) {

            ps.setLong(1, id);
            numberOfRowsAffected = ps.executeUpdate();
            
            if (numberOfRowsAffected == 0) {
                throw new AppException(ErrorCode.ADDRESS_DELETION_FAILS);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_DELETION_FAILS, e);
        }
    }
    
    public ArrayList<Address> search(String street, String city, String postalCode) {
        
        Address address = null;
        ArrayList<Address> addresses = new ArrayList<>();

        try (PreparedStatement ps = ThreadPool.get().prepareStatement(QueryStatement.ADDRESS_SEARCH);) {
            ps.setString(1, "%" + street + "%");
            ps.setString(2, "%" + city + "%");
            ps.setString(3, "%" + postalCode + "%");
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                address = readAddress(result);
                addresses.add(address);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.SEARCHING_ADDRESS_FAILS, e);
        }

        return addresses;
    }
    
    public Address readAddress(ResultSet result) {
        Address address = null;
        try {
            address = new Address(result.getString("street"), result.getString("city"),
                    result.getInt("postal_code"));
            address.setId(result.getLong("id"));
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILS, e);
        }
        return address;
    }
    
    public long getAddressId(Address address, Connection con) {
      ResultSet result;
      long id = 0;

      try (PreparedStatement ps = con.prepareStatement(QueryStatement.ADDRESS_UNIQUE)) {

          ps.setString(1, address.getStreet());
          ps.setString(2, address.getCity());
          ps.setLong(3, address.getPostalCode());
          result = ps.executeQuery();
          if (result.next()) {
              id = result.getLong("id");
          }

      } catch (Exception e) {
          throw new AppException(ErrorCode.FAILED_TO_CHECK_ADDRESS, e);
      }
      return id;
  }

}
