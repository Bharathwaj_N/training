public class NewCar implements Vehicle{
    int gear;
    int speed;
    
    public void changeGear(int newGear) {
        this.gear = newGear;
    }
    
    public void travelSpeedUp(int increaseSpeed) {
        speed = speed + increaseSpeed;
    }
    
    public void printStatus() {
        System.out.println("the new gear is: " + " " + this.gear + " " + "and the current speed is: " + " " + speed);
        }
}