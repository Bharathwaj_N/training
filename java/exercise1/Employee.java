import com.bank.training.core.Person;

public class Employee extends Person {
    protected int employeeId;
    
    public Employee(String personName, String personGender, int id) {
        super(personName, personGender);
        this.employeeId = id;
    }
    public void work() {
        if(employeeId == 0) {
            System.out.println("No such employee is Working");
        }
        else {
            System.out.println(this.name + " " + "is working as a employee");
        }
    }
    
    public static void main(String[] args) {
        Person person = new Employee("Bharath", "Male", 3003);
        Person employee = new Employee("Kamdhev", "Male", 0);
        person.work();
        employee.work();
        person.changeName("Bharathwaj");
        System.out.println(person.toString());
    }
}