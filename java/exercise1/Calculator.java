package com.math.training.calculate;//create package

import java.util.Scanner;

public class Calculator {
    public void addition(int a, int b) {//method signature
        System.out.println(a+b);
    }

    private void subtraction(int a, int b) {
        System.out.println(a-b);
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        calculator.addition(a,b);
        calculator.subtraction(a,b);
    }
}