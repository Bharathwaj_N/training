interface Vehicle {   // public access mode
// Abstract method are by deafault
    void changeGear(int a);
    void travelSpeedUp(int a);
}
