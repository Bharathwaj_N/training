package com.university.training.core;

import java.util.Scanner;

import com.math.training.calculate.Calculator;//fully qualified name

public class Department {
    private String departmentName;
    public int departmentNumber;
    public Department() {
        //non-parameterized constructor
    }

    protected Department(int code, String name) {//Parameterised contructor
        departmentNumber = code;
        departmentName = name;
    }

    {
        departmentName = "EEE";
        departmentNumber = 01;
    }

    public static void main(String[] args) {
        Department dept = new Department();
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        System.out.println(" Department Name: " + dept.departmentName + " Department Number: " + dept.departmentNumber);//prints "EEE"
        Department dept1 = new Department(02,"ECE");
        System.out.println(" Department Name: " + dept.departmentName + " Department Number: " + dept.departmentNumber);//prints '1'
        System.out.println(" Department Name: " + dept1.departmentName + " Department Number: " + dept1.departmentNumber);//prints "EEE"
        com.math.training.calculate.Calculator calculator = new com.math.training.calculate.Calculator();
        calculator.addition(a,b);
        calculator.subtractrion(a,b);//prints error because private member cannot be accessed by outside the class
    }
}