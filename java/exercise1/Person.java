package com.bank.training.core;

public abstract class Person {       //abstract class
    protected String name;
    public String gender;
    
    public Person(String personName, String personGender) {
        this.name = personName;
        this.gender = personGender;
    }
    
      // abstract method
    public abstract void work();
    
    public String toString() {   // returns string for the object
        return "Name = " + this.name + "Gender = " + this.gender;
    }
    
    public void changeName(String newName) {
        this.name = newName;
    }
}