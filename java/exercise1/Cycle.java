public class Cycle implements Vehicle {
    int gear;
    int speed;
    
    // only abstract methods are created
    public void changeGear(int gear1) {
        this.gear = gear1;
    }
        
    public void travelSpeedUp(int speedIncrement) {
        speed = speed + speedIncrement;
        }
        
    public void printStatus() {
        System.out.println("the new gear is: " + " " + this.gear + " " + "and the current speed is: " + " " + speed);
        }
}
