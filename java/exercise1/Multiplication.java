package com.math.training.multiplication;

import java.util.Scanner;

import com.math.training.calculate.Calculator;

class Multiplication extends Calculator {
    public static void main(String[] args) {
        Multiplication multiply = new Multiplication();
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        System.out.println(multiply.product(x,y));//prints error due to private access modifier used in the class Caculator.
        multiply.add(x,y);
    }
}