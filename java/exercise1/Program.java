import java.util.Scanner;
public class Program {
    
    public static void main(String[] args) {
        // object for the class NewCar
        NewCar car = new NewCar();
        Scanner sc = new Scanner(System.in);
        int speed = sc.nextInt();
        int gear = sc.nextInt();
        car.changeGear(gear);
        car.travelSpeedUp(speed);
        System.out.println("the current status of a car is ");
        car.printStatus();
        //object creation for the Class Cycle
        Cycle cycle = new Cycle();
        int cycleSpeed = sc.nextInt();
        int cycleGear = sc.nextInt();
        cycle.changeGear(cycleGear);
        cycle.travelSpeedUp(cycleSpeed);
        System.out.println("the current status of cycle is ");
        cycle.printStatus();
    }
}