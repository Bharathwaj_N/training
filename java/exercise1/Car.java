public class Car {      // Outer Class
    // Outer Class Properties
    public String carType;
    public String carName;

    // Outer Class Constructor
    public Car(String type, String name) {
        this.carName = name;      // Baleno
        this.carType = type;      // 2 Stroke
    }
    
    private void printCarName() {     // Outer Class Method
        System.out.println(this.carName);
    }
    
    public class Engine {    // Nested or Inner Class
        String engineType;   // Nested Class Property
        
        public void printEngine() {   // Nested Class Method
            if(Car.this.carType.equals("2 stroke")) {     // condition true

                if(Car.this.carName.equals("Innova")) {   // condition false
                    this.engineType = "Smaller";
                }
                
                else {
                    this.engineType = "Bigger";         // executed
                }
                
            } 
            else {
                this.engineType = "Bigger";
            }

        }

        public String printEngineType() {   // Inner class Method
            return this.engineType;        // return bigger
        }

    }
}

