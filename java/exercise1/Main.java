public class Main {

    public static void main(String[] args) {
        Car car = new Car("2 stroke", "Baleno");    // Object for Outer class Car
        car.printCarName();                         // prints error due to the private access of the class
        Car.Engine engine = car.new Engine();       // Object for Inner Class Engine
        engine.printEngine();
        System.out.println("Engine Type of this car type is: " + engine.printEngineType());
        Car car1 = new Car("2 stroke", "Innova");
        Car.Engine engine1 = car1.new Engine();
        engine1.printEngine();
        System.out.println("Engine Type of this type of car is: " + engine1.printEngineType());
    }

}