package com.java.training.core;

/*
 Requirement:
  print the type of the result value of following expressions
  - 100 / 24
  - 100.10 / 10
  - 'Z' / 2
  - 10.5 / 0.5
  - 12.4 % 5.5
  - 100 % 56
Entity:
    DataType
Function Signature:
    public static void main(String[] args)
Jobs to be done:
    1) Create object variable and assign the given expression.
    2) print the type of the result value of the object variable.
 */

public class DataTypeDemo {

    public static void main(String[] args) {
        Object object1 = 100 / 24;
        System.out.println(object1.getClass().getName());
        
        Object object2 = 100.10 / 10;
        System.out.println(object2.getClass().getName());
        
        Object object3 = 'Z' / 2;
        System.out.println(object3.getClass().getName());
        
        Object object4 = 10.5 / 0.5;
        System.out.println(object4.getClass().getName());
        
        Object object5 = 12.4 % 5.5;
        System.out.println(object5.getClass().getName());
        
        Object object6 = 100 % 56;
        System.out.println(object6.getClass().getName());
    }
}
