package com.java.training.core;

/*
Requirement:
    To find the value of the following expression, and to say why
     Integer.valueOf(1).equals(Long.valueOf(1)).

Entity:
    ValueOfExpression.

Function Declaration:
    public static void main (String[] args).
    valueOf().

Jobs to be Done:
     1. print the given expression.
*/

/* Answer of the given expression is false. this is proved in the below program. Because the two
   objects Integer and Long have different types.
*/

// Program
public class ValueOfExpression {

    @SuppressWarnings("unlikely-arg-type")
    public static void main(String[] args) {
        System.out.println(Integer.valueOf(1).equals(Long.valueOf(1)));
    }
}
