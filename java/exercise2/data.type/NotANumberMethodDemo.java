package com.java.training.core;

/*
Requirement:
    To find a Double method can be used to detect whether a floating-point number has the special
    value Not a Number (NaN).

Entity:
    NotANumberMethodDemo.

Function Declaration:
    public static void main(String[] args).
    isNaN()

Jobs to be Done:
     1) Create double variable and assign expressions.
    2) Check the expression return NOt a Number 
        2.1) If yes,print true.
        2.1) If no,print false.
*/

public class NotANumberMethodDemo {

    public static void main(String[] args) {
        Double number1 = new Double( 7.0 / 0.0 );
        Double number2 = new Double( 0.0 - 0.0 );
        Double number3 = new Double( 7.0 * 0.0 / 0.0 );
        System.out.println( number1 + " = " + " " + number1.isNaN() );
        System.out.println( number2 + " = " + " " + number2.isNaN() );
        System.out.println( number3 + " = " + " " + number3.isNaN() );
    }
}
