package com.java.training.core;

/*
Requirement:
    To demonstrate overloading with Wrapper types.

Entity:
    WrapperOverLoading

Function declaration:
    check(Integer),check(Float),check(Double),check(Long)
    public static void main(String[] args)

Jobs to be done:
    1.Create a object for the class.
    2.Call the methods with different data type values like integer,float,double,long.
    3.Print the respective values of wrapper types.
*/

//Program:
public class WrapperOverLoading {

    public void check( Integer number ) {
        System.out.println( "Integer : " + number );
    }

    public void check( Float number ) {
        System.out.println( "Float : " + number );
    }

    public void check( Double number ) {
        System.out.println( "Double : " + number );
    }

    public void check( Long number ) {
        System.out.println( "Long : " + number );
    }

    public static void main(String[] args) {
        WrapperOverLoading value = new WrapperOverLoading();
        value.check(new Integer(50));
        value.check(new Float(12.5f));
        value.check(new Double(6.57839d));
        value.check(new Long(567890855089L));
    }
}