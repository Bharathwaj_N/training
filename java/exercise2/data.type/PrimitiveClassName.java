package com.java.training.core;

/*
Requirement:
    To print the ClassNames of the Primitive Datatype.

Entity:
    1.PrimitiveClassName

Function Declaration:
    public static void main(String[] args)
    getname()

Jobs to be Done:
    1.Print the respective class name of primitive data type.
*/

public class PrimitiveClassName {

    public static void main(String[] args) {
        String intClassName = int.class.getName();
        System.out.println("ClassName of Int : " + intClassName);
        
        String charClassName = char.class.getName();
        System.out.println("ClassName of Char : " + charClassName);
        
        String doubleClassName = double.class.getName();
        System.out.println("ClassName of double : " + doubleClassName);
        
        String floatClassName = float.class.getName();
        System.out.println("ClassName of float : " + floatClassName);
    }
}