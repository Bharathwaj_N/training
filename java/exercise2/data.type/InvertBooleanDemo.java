package com.java.training.core;

/*
Requirement:
    To find which operator would use to invert the value of the boolean.

Entity:
    InvertBooleanDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1. Assign the boolean expression to the boolean variable.
    2. print the inverted value of a boolean variable.
    
Answer:
    The logical complement operator "!" would be used.
*/

//program:
public class InvertBooleanDemo {
    
    public static void main(String[] args) {
        boolean value = true;
        value = !value;
        System.out.println("the inverted value of boolean is: " + " " + value);
    }
}