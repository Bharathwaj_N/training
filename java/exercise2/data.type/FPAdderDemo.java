package com.java.training.core;

/*
Requirement:
    To create a program that is similar to the previous one but instead of reading integer arguments,
    it reads floating-point arguments.It displays the sum of the arguments, using exactly two digits
    to the right of the decimal point.

Entity:
    FPAdderDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1. Get the command line arguments and convert that into float value.
    2. Check whether the length of the argument is more than two
           2.1) if it is less than one, print "Error".
           2.2) if it is more than two, then
                    2.21) declare the double value and initialize it to zero.
                    2.22) for each element in the command line argument,
                              2.22.1) add the previous sum with the present element in the command
                                       line argument.
    3. Now format the sum value based on given and print the result.  
*/


import java.text.DecimalFormat;

public class FPAdderDemo {

    public static void main(String[] args) {
        Float.parseFloat(args[0]);
        Float.parseFloat(args[1]);
        Float.parseFloat(args[2]);
        
        if ( args.length < 2 ) {
            System.out.println( "Error" );
        } else {
        double sum = 0.0;

        for ( int i = 0; i < args.length; i++ ) {
                sum += Double.valueOf(args[i]).doubleValue();
        }

        DecimalFormat myFormatter = new DecimalFormat( "###,###.##" );
        String outputValue = myFormatter.format(sum);
            System.out.println( outputValue );
        }
    }
}