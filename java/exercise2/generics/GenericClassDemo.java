public class GenericClassDemo {

    public static void genericTest() {
        GenericClass<Integer> integerObject = new GenericClass<>(88);
        integerObject.showType();
        int value = integerObject.getObject();
        System.out.println( "value" + " " + value );
        System.out.println( "--------------------------------" );

        GenericClass<String> stringObject = new GenericClass<>( "the name is Ram Vijay" );
        stringObject.showType();
        String string = stringObject.getObject();
        System.out.println( " the String is " + " " + string);
    }

    public static void main(String[] args) {
        genericTest();
    }
}