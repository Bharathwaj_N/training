// Consider the Followig program using generics:
import java.util.ArrayList;
import java.util.Iterator;

public class GenericExample {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(10);
        list.add(20);
        //list.add("ram")   // Shows a compile time error not at runtime.
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Integer integer = (Integer) iterator.next();
            System.out.println(integer);
        }
    }
}
/* Note that generics allows only an object types not a primitive type and it is homogeneous. And 
    that <Integer> denotes the type parameter for ArrayList.*/