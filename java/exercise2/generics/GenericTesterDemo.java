
public class GenericTesterDemo {
   public static void main(String[] args) {
      Box<Integer, String> box = new Box<Integer, String>();
      box.add(Integer.valueOf(10),"Hello World");
      System.out.printf("Integer Value : " box.getFirst());
      System.out.printf("String Value : " + box.getSecond());

      Box<String, String> box1 = new Box<String, String>();
      box1.add("Message","Hello World");
      System.out.printf("String Value :" + box1.getFirst());
      System.out.printf("String Value :" +  box1.getSecond());
   }
}

