// Writing Generic classes( Unbounded Type)
public class GenericClass<T> {

    T dataObject;

    public GenericClass(T object) {
        this.dataObject = object;
    }

    public T getObject() {
        return dataObject;
    }

    public void showType() {
        System.out.println( "The type of T is " + " " + dataObject.getClass().getName() );
    }
}