class IntegerHolder {
    private Integer s;
    public Holder(Integer s) {
        this.s = s;
    }
    public Integer getObject() {
        return s;
    }
    public void printObject() {
        System.out.println(s);
    }
}