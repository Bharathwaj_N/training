// Writing Generic Classes using multibounded parameter type.
public class GenericNumberClass<T extends Number> {

    T numberObject;

    public GenericNumberClass( T number ) {
        this.numberObject = number;
    }

    public double printReciporcal() {
        return 1/numberObject.doubleValue();
    }

    public double printFraction() {
        return numberObject.doubleValue() - numberObject.intValue();
    }

    public static void main(String[] args) {
        GenericNumberClass<Integer> integerValue = new GenericNumberClass<>(18);
        System.out.println( "Reciporcal of integer is " + " " + integerValue.printReciporcal() );
        System.out.println( "Fraction of Integer value is " + " " + integerValue.printFraction() );
        System.out.println( "-------------------------");

        GenericNumberClass<Double> doubleNumber = new GenericNumberClass<>(5.25);
        System.out.println( "Reciporcal of double is " + " " + doubleNumber.printReciporcal() );
        System.out.println( "Fraction of double value is " + " " + doubleNumber.printFraction() );

        /* The following line will print error
        GenericNumberClass<String> string = GenericNumberClass<>("5.25");
        Because the type parameter extends number so it cannot allow String
        */
    }
}