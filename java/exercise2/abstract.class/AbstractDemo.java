package com.java.training.core;

import java.util.Scanner;

/*
Requirement: 
    To demonstrate the abstract class using the class shape that has two methods to
    calculate the area and perimeter of two classes named the Square and the Circle extended from the
    class Shape.
  
Entity: 
    abstract class Shape. 
    Class Square extends Shape.  
    Class Circle extends Shape.
  
Function Declaration: 
    public void printArea(); 
    public void print perimeter(); 
    public static void main(String[] args).
  
Jobs to be done: 
    1. Declare the abstract method printArea and printPerimeter.
    2. Declare a private variable.
    3. Declare a constructor with a parameter.
          3.1)Assign the parameter value to the variable.
    4. Declare a method.
          4.1)Print the area of the square.
    5. Declare a method.
          5.1)Print the perimeter of square.
    6. Declare the variable radius.
    7. Declare a constructor with one parameter as type double.
          7.1)Assign the parameter value to radius.
    8. Declare a method.
          8.1)Print the area of circle.
    9. Declare a method.
          9.1)Print the perimeter of the circle.
    10. Create a Scanner object.
    11. Get two double inputs from the user using scanner object.
    12. Create a object for Square class and pass the length value in the constructor.
    13. Invoke the printArea and printPerimeter method.
    14. Create a object for Circle class and pass the radius value in the constructor.
    15. Invoke the printArea and printPerimeter method.
 
 */

abstract class Shape {

    public abstract void printArea();
    public abstract void printPerimeter();
}

class Circle extends Shape {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public void printArea() {
        System.out.println(3.14 * radius * radius);
    }

    public void printPerimeter() {
        System.out.println(2 * 3.14 * radius);
    }
}

class Square extends Shape {

    private double side;

    public Square(double side) {
        this.side = side;
    }

    public void printArea() {
        System.out.println(side * side);
    }

    public void printPerimeter() {
        System.out.println(4 * side);
    }
}

public class AbstractDemo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double side = scanner.nextDouble();
        double radius = scanner.nextDouble();
        Square square = new Square(side);
        square.printArea();
        square.printPerimeter();
        Circle circle = new Circle(radius);
        circle.printArea();
        circle.printPerimeter();
        scanner.close();
    }
}
