package com.java.training.core;

/*
Requirement:
    To find the output of the following program after each numbered line.

Entity:
    ComputeResult

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1) A string value is initialized.
    2) Another string builder is assigned.
    3) The given conditions are checked and printed.
*/

/*
public class ComputeResult {

        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

    /1/   result.setCharAt(0, original.charAt(0));    //  si
    /2/   result.setCharAt(1, original.charAt(original.length()-1));    //  se
    /3/   result.insert(1, original.charAt(4));    //  swe
    /4/   result.append(original.substring(1,4));    //  sweoft
    /5/   result.insert(3, (original.substring(index, index+2) + " "));    //  swear oft

            System.out.println(result);
        }
}*/