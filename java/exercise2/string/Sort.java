package com.java.training.core;

/*
Requirement:
    To sort and print following String[] alphabetically ignoring case. Also convert and print even
    indexed Strings into uppercase.
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }

Entity:
    Sort

Function declartion:
    There is no function is declared in tbis program

Jobs to be done:
    1) To print the given city names in sorted order, the city names are assigned to the String list.
    2) The list is sorted and printed.
    3)For each city name, 
        3.1)Check if the city name is present in even index.
            3.1.1)If yes, convert that particular city into UpperCase.
*/

import java.util.Arrays;
import java.util.List;
import java.util.Collections;

public class Sort {

    public static void main(String[] args) {
        String[] area = { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        List <String> list = Arrays.asList(area);
        Collections.sort( list, String.CASE_INSENSITIVE_ORDER );
        System.out.println( list );
        for ( int i = 0; i < area.length; i++ ) {
            if ( i%2 == 0 ) {
                area[i] = area[i].toUpperCase();
            }
        }

        System.out.println(Arrays.toString(area));
    }
}
