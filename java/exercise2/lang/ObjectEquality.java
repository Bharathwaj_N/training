package com.java.training.core;

/*
Recuirement:
    To demonstrate object equality using Object.equals() vs ==, using String objects.

Entity:
    ObjectEquality

Function Declaration:
    No Function.

Jobs To be Done:
   1)Two string value is assigned to two string variables.
    2)Check two string values with "==" operator.
        2.1)Whether both the string values are equal and has same address, print true.
        2.2)Otherwise, print false.
    3)Check two string values with equals().
        3.1)Whether both the string values are equal, print true.
        3.2)otherwise, print false.
*/
public class ObjectEquality { 

    public static void main(String[] args) {
        String string1 = new String( "HELLO" ); 
        String string2 = new String( "HELLO" ); 
        System.out.println( string1 == string2 ); 
        System.out.println( string1.equals(string2) ); 
    } 
}