package com.java.training.core;

/*
Requirement:
    To print the absolute path of the .class file of the current class.

Entity:
    CurrentDirectory.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1.Get the property of absolute path of the Current directory file and assigned to the string variable.
    2.Print the Current directory of the file from the String variable. 
*/

public class CurrentDirectory {

    public static void main(String[] args) {

        String path = System.getProperty("user.dir");
        System.out.println("Working Directory = " + path);
    }
}
