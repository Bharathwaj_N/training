package com.java.training.core;

/*
Requirement:
    To find fibonacci series using while loop.

Entity:
    Here the entity FibonacciUsingWhile has been given.

Function Declaration:
    Here there is no function is declared.

Jobs to be Done:
    1. Declare the class FibonacciUsingWhile.
    2. First declare the range to which the Series to be printed and assign the first two numbers of
       the series under two different variables as firstNumber and secondNumber.
    3. Then use while loop. Assign the value to the new variable i. Continue the process till the 
       condition meets value <= number.
    4. Now add the values of firstNumber and secondNumber and store it in new variable sum.
    5. Now assign the value of secondNumber to firstNumber and the value of b to sum and then
       increments the value of i.
    6. Now print firstNumber.
*/

import java.util.Scanner;

public class FibonacciUsingWhile {

    public static void main(String[] args) {
        int number;
        int sum;
        int firstNumber = 0;
        int secondNumber = 1;
        int value = 1;
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();
        while ( value <= number ) {
            sum = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = sum;
            value++;
            System.out.print( " " + firstNumber );
            scanner.close();
        }
    }
}