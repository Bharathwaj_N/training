package com.java.training.core;

/*
Requirement:
    To write the test program consisting the following codes snippet and make the value of aNumber
    to 3 and find the output of the code by using proper line space amd breakers and also by using
    proper curly braces. The following snippet is given as
     if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
     else System.out.println("second string");
     System.out.println("third string");

Entities:
    public class Test is the entity used here.

Function Declaration:
    There is no function declared in this program.

Jobs To Be Done:
    1) Declare the integer variable and assign 3 to it.
    2) Check whether the number is greater than or equal to 0.
           2.1)if true, then check whether the number is equal to 0.
                   2.1.1)if true, then print "First String".
           2.2)if false, then print "Second String".
    3) Print "Third String".
*/

public class Test {

    public static void main(String[] args) {
        int aNumber = 3;
        if ( aNumber >= 0 ) {
            if ( aNumber == 0 ) {
                System.out.println( "First String" );
            }
         } else {
               System.out.println( "Second String" );
        }

        System.out.println( "Third String" );
    }
}