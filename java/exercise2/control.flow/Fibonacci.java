package com.java.training.core;

/*
Requirement:
    To find fibonacci series using for loop.

Entity:
    Here the entity Fibonacci has been given.

Function Declaration:
    Here there is no function is declared.

Jobs to be Done:
    1) Declare two integer variable rangeValue and sum.
    2) Declare a integer variable number1 and assign 0 to it.
    3) Declare a integer variable number2 and assign 1 to it.
    4) Create a Scanner object.
    5) Get the integer input from user and assign it to rangeValue.
    6) For each value in the range 1 to rangeValue.
           6.1)add number1 and number2 and assign it to sum.
           6.2)assign the value of number2 to number1.
           6.3)assign the value of sum to number2.
           6.4)Print number1 and a white space.
*/

import java.util.Scanner;

public class Fibonacci {

    public static void main(String[] args) {
        int number;
        int sum;
        int firstNumber = 0;
        int secondNumber = 1;
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();
        for ( int value = 1; value <= number; value++ ) {
            sum = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = sum;
            System.out.print( firstNumber + " " );
            scanner.close();
         }
    }
}