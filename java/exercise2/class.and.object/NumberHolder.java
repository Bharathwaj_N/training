package com.java.training.core;

/*
Requirement:
    To write code that creates an instance of the class and initializes its two member variables 
    with provided values,and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }

Entities:
    It has the class name NumberHolder

Function Declaration:
    There is no function is declared.

Jobs To Be Done:
    1)Consider the given program.
    2)Inside the class declare the main function program.
    3)Create the object numberHolder for the class NumberHolder to perform the actions.
    4)Initialize the value for the variables( 18 for int and 8.4f for float)
    5)Print the values to display the instance variable values
*/

public class NumberHolder {

    public int anInt;
    public float aFloat;

    public static void main(String[] args) {
        NumberHolder numberHolder = new NumberHolder();
        numberHolder.anInt = 18;
        numberHolder.aFloat = 8.4f;
        System.out.println( numberHolder.anInt );
        System.out.println( numberHolder.aFloat );
    }
}