package com.java.training.core;

/*
Requirements:
    To check and correct the error in the following program
    public class SomethingIsWrong {
       public static void main(String[] args) {
           Rectangle myRect;
           myRect.width = 40;
           myRect.height = 50;
           System.out.println("myRect's area is " + myRect.area());
       }
    }

Entities:
    It has the class SomethingIsWrong.

Function Declaration:
    public int area()

Jobs To Be Done:
    1. Declare two integer variables.
    2. Define a method.
          2.1)return the product of two above declared variables.
    3. Create a object of this class.
    4. Assign the values of the two declared variables.
    5. Print the area by invoking the method
*/

//The Corrected Program:
public class Rectangle {

    public int width;
    public int height;

    public int area() {
        return width * height;
    }

    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println( "myRect's area is " + myRect.area() );
    }
}