/*
Requiremnts:
    To find the output of the following program
    IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);

Entities:
    It requires the class IdentifyMyParts.

Function Declaration:
    Here there is no function is declared.

Jobs To Be Done:
    1. Consider the given program.
    2. Checking the values of x and y for the objects a and b.
    3. Print the resultant values for each variables.
    4. Print the class variable
*/

/*
Output:
a.y = 5
b.y = 6
a.x = 2
b.x = 2
IdentifyMyParts.x = 2
*/
