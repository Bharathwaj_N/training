/*
Requirment:
    To find the class and instance variables in the following program.
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }

Entities:
    Here the class IdentifyMyParts is used.

Function Declaration:
    There is  no function is declared in this program.

Jobs to be done:
    1. Consider the given class given.
    2. Identify the class variable(The variable declared in static).
    3. Answer it for the given question.
    4. Then identify an instance variable in the given class.
    5. Answer it for the given question.
*/

/* 
Answer:
Class variables is x(static int x = 7).
instance variable is y.(int y = 3).
*/