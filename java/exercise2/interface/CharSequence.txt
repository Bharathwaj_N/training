/*
Requirement:
    To find what method would a class that implements the java.lang.CharSequence interface have to
    implement.

Entity:
    CharSequence.

Function Declaration:
    charAt(), length(), subSequence(), toString()

Jobs to be Done:
    To answer the given question.
*/

Answer:
    The method that thed class implements java.lang.CharSequence have to implement are charAt(),
    length(), subSequence(), toString().