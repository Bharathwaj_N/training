package com.java.training.core;

/*
Requirement:
    To compare the enum values using equals method and == operator.

Entity:
    public class EnumDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1)Create a variable and assign null.
    2)Create a enum type Mentor and store the values.
    3)Create a enum type Student and store the values.
    4)Compare using "==" operator.
        4.1)Check if the two Strings point to the same address,then print true.
        4.2)otherwise print false.
    5)Compare using equals.
        5.1)check if the two strings are equal,then print true.
        5.2)otherwise print false.
    6)Add a new element to the enum Student.
    7)Compare using "==" operator.
        7.1)Check if the two Strings point to the same address,then print true.
        7.2)otherwise print false.
    8)Compare using equals.
        8.1)check if the two strings are equal,then print true.
        8.2)otherwise print false.
*/

//Answer:

public class EnumDemo {

    public enum Day {
        Monday,
        Tuesday,
        Wednesday,
        Thrusday,
        Friday,
        Saturday,
        Sunday
    }

    public enum Month {
        January,
        Feburary,
        March,
        April,
        June,
        July,
        August,
        September
    }

    public static void main(String[] args) {
        Day day = null;
        Day monday = Day.Monday;
        System.out.println(day == monday);                // prints false
        // shows null pointer exception because null cannot be called by a method.
        //System.out.println(day.equals(monday));
        System.out.println(day == monday);      // shows error
        //System.out.println(Month.January.equals(monday)); // prints false
        /*Month month = Month.April;
        System.out.println(month == Month.April);             // prints true
        System.out.println(month.equals(Month.April)); */       // prints true
    }
}