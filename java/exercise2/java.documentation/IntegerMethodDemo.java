package com.java.training.core;

/*
Requirement:
    To find Integer method can you use to convert an int into a string that expresses the number in
    hexadecimal.

Entity:
    IntegerMethodDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1. Create integer variable and assign the integer value to it.
    2. Print the hexadecimal value of the given integer value.
*/

import java.util.Scanner;

public class IntegerMethodDemo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        System.out.println( "HexaDecimal representation is = "+ Integer.toHexString(number) );
        scanner.close();
    }
}