package com.java.training.core;

/*
Entities:
    Cat.

Function Declaration:
    public void sound().

Jobs to be done:
   1)Declare the method sound.
        1.1)print the statement.
*/

public class Cat extends Animal {

    public void sound() {
        System.out.println("the cat meows");
    }
}