package com.java.training.core;

/*
Entities:
   class Dog extends Animal.

Function Declaration:
    public void sound().

Jobs to be done:
   1) Declare the method sound().
    2) Print the the statement.
*/

public class Dog extends Animal {

    public void sound() {
        System.out.println("the dog barks");
    }
}