package com.java.training.core;

/*
Entities:
    Snake.

Function Declaration:
    public void sound().

Jobs to be done:
    1. Declare the class Snake.
    2. call the method sound().
    3. Print the the statement as given.
*/

public class Snake extends Animal {

    public void sound() {
        System.out.println("the snake crawls");
    }
}