package com.java.training.core;

/*
Entities:
    InheritanceDemo

Function Declarartion:
    public static void main(String[] args)

Jobs to be done:
    1. Get a scanner input as scanner for a integer variable kilometer.
    2. Now create a object for the classes Animal, Dog, Cat, Snake as animal, dog, cat, snake.
    3. Now call the functions sound() for the objects cat and dog as cat.sound() and dog.sound()
        This is an example for overriding.
    4. After this call the method run(int kilometers) by an object animal. This is an example for
       method overloading.
*/

import java.util.Scanner;

public class InheritanceDemo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int kilometer = scanner.nextInt();
        Animal animal = new Animal();
        Animal dog = new Dog();
        Animal cat = new Cat();
        //Animal snake = new Snake();
        cat.sound();
        dog.sound();
        animal.run( kilometer );
        scanner.close();
    }
}