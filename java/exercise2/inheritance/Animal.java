package com.java.training.core;

/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entities:
    Animal

Function Declaration:
    public void sound()
    public void run()
    public void run(int kilometer)

Jobs to be Done:
     1) Declare the method sound.
        1.1)print the statement.
    2) Declare the method moves.
        2.1)print the statement.
    3) Declare the method moves with parameter 
        3.1)print the statement.
*/

public class Animal {

    public void sound() {
        System.out.println(" This is the parent class");
    }
    
    public void run() {
        System.out.println("the animal is running");
    }
    
    public void run(int kilometer) {
        System.out.println("the animal runs without any tired");
    }
}