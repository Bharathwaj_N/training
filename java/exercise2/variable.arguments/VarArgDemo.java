/*
Requirement:
    To demonstrate the method overloading using varArgs.

Entity:
    VarArgDemo.

Function Declaration:
    public static void main(String[] args).
    public void test(int ... numbers)
    public void test(String message, int numnbers).

Jobs to be done:
    1. Declare the class VarArgDemo as public.
    2. Declare the function named test and integer values as a arguements.
    3. Print the number of integer values given and print that values given sby using for each loop.
    4. Now declare the same method name as test now pass String message and group of integer values
        as a arguements.
    5. Now print the message given and the length of the integer values.
    6. Print the the given integer type using for each loop.
*/

public class VarArgDemo {

    public void test( int ... numbers ) {
        System.out.println( "Number of intger values: " + " " + numbers.length + "The numbers: " );
        for ( int values : numbers ) {
            System.out.print("the values are: " + " " + values);
            System.out.println();
        }
    }
    
    public void test(String message, int ... numbers) {
        System.out.println( "the message " + message + " the number of values" + numbers.length );
        for ( int integerValues : numbers ) {
            System.out.println(" the integer values are " + " " + integerValues + " ");
        }
    }

    public static void main(String[] args) {
        VarArgDemo variable = new VarArgDemo();
        variable.test("the message is deleted", 10,20,30);
    }
}