/*
Requirement:
    To write a program 
    - to demonstrate absolute and relative path
    - to obtain current directory and parent directory using codes(. & ..)
    - to demonstrate toAbsolutePath(), normalize(), getName(), getFileName() and getFileCount()

Entity:
    NioPath
    
Functional Declaration:
    public static void main(String[] args) throws IOException
    
Jobs to be done:
    1. Create an object of the path for destination.txt
        1.1 Get the file name of the path.
        1.2 Print the file name.
    2. Declare a string containing a relative path of a file.
        2.1 Print the relative path.
        2.2 Convert the relative to absolute path.
        2.3 Print the absolute path.
    3. Declare a directory to store the count of directory of the file.
        3.1 For each directory, print the directory name.
    4. Print the normalized path of the file.
    5. Print the current directory of the file.
    6. Print the parent directory of the file.
    
Pseudo code:
class NioPath {
    public static void main(String[] args) throws IOException {
        
        Path path = Paths.get(
                "C:\\Users\\Win 10 Pro\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\nio\\destination.txt");

        Path fileName = path.getFileName();

        System.out.println("FileName: " + fileName.toString());
        
        String fileName1 = "source.txt";   
        Path path1 = Paths.get(fileName1);
        System.out.println("Relative path : " + path1);
        System.out.println("Absolute path : " + path1.toAbsolutePath());
       
        int directory = path.getNameCount();
        for(int count = 0; count < directory; count++) {
            System.out.println(" Element " + count + " is " + path.getName(count));
        }
        
        System.out.println("Normalized Path:" + path.normalize());
        
        String path2 = Paths.get("").toAbsolutePath().toString();
        System.out.println("Current Working Directory : " + path2);
        
        System.out.println("Parent directory : " + path.getParent());
        

    }
}

        
*/
package com.java.training.NIO;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NioPath {
    public static void main(String[] args) throws IOException {

        // create object of Path
        Path path = Paths.get(
                "C:\\Users\\Win 10 Pro\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\nio\\destination.txt");

        // call getFileName() and get FileName path object
        Path fileName = path.getFileName();

        // print FileName
        System.out.println("FileName: " + fileName.toString());

        // Absolute path and Relative path
        String fileName1 = "source.txt"; // Relative path
        Path path1 = Paths.get(fileName1);
        System.out.println("Relative path : " + path1);
        System.out.println("Absolute path : " + path1.toAbsolutePath());

        // System.out.println("Name:" + path.getName(1));

        System.out.println("Count:" + path.getNameCount());

        int directory = path.getNameCount();
        for (int count = 0; count < directory; count++) {
            System.out.println(" Element " + count + " is " + path.getName(count));
        }

        // Normalized file path
        System.out.println("Normalized Path:" + path.normalize());

        // Current directory
        String path2 = Paths.get("").toAbsolutePath().toString();
        System.out.println("Current Working Directory : " + path2);

        // Parent directory
        System.out.println("Parent directory : " + path.getParent());


    }
}
