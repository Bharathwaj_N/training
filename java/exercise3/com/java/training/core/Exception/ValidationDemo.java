package com.java.training.core.Exception;

/*
Requirement:
    To demonstrate the importance of Validation.

Entity:
    public class ValidationDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Declare an integer variable number and initialize it to zero.
    2. Declare a String variable string.
    3. Declare a boolean variable valid and initialize it to false.
    4. Get the user input in the form of string.
    5. Check whether the user input until the value of valid becomes true.
           5.1) try to convert the string input for a number to integer and check if the value of valid becomes true
                      5.11) If valid is true then exit the loop and print the number
                      5.12) if valid is false catch the exception and print the message "oops!! Enter an integer value"*/

import java.util.Scanner;

public class ValidationDemo {
    
    public static void main(String[] args) {
        int number = 0;
        String string;
        boolean valid = false;
        
        // setup Scanner.
        Scanner scanner = new Scanner(System.in);
        // keep looping until valid input
        while (valid == false) {
            System.out.print("Enter an integer value ");
            // Grab input from user for 6string
            string = scanner.nextLine();
            // try to convert string to integer
            try {
                number = Integer.parseInt(string);
                valid = true;
            } catch (NumberFormatException e) {
                System.out.println("oops!! Enter an integer value");
            }
        }
        
        System.out.println("the number you entered is " + number);
        scanner.close();
    }
}
