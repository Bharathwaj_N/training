package com.java.training.core.Exception;

/*
Requirement:
    To handle the given program.
       public class Exception {  
           public static void main(String[] args) {
               int arr[] ={1,2,3,4,5};
               System.out.println(arr[7]);
           }
       }
Entity:
    public class HandleDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Assign the values to an integer array named array to it.
    2. Print the given index value of an array.
    3. If that throws an exception catch that exception.
    
Pseudo code:
class HandleDemo {
    
    public static void main(String[] args) {
        try {
            int[] array = {1,2,3,4,5,6};
            System.out.println(array[7]);
        } catch (ArrayIndexOutOfBoundsException e1) {
            System.out.println("Not in the index level");
        } catch (Exception  e2) {
            System.out.println("The exception is produced");
        }
    }
}
        
*/

public class HandleDemo {
    
    public static void main(String[] args) {
        try {
            int[] array = {1,2,3,4,5,6};
            System.out.println(array[7]);
        } catch (ArrayIndexOutOfBoundsException e1) {
            System.out.println("Not in the index level");
        } catch (Exception  e2) {
            System.out.println("The exception is produced");
        }
    }
}
            
        