package com.java.training.core.Exception;

/*
Requirement:
    To write a program using try and catch.

Entity:
    public class ListOfNumber.

Function Declaration:
    public static void main(String[] args)
    public voidwriteList()

Jobs to be done:
    1. Declare an integer array whose name is arrayOfNumbers and get 10 values for it.
    2. Try to access the 11th element of arrayOfNumbers,
           2.1) If that throws exception catch that exception and print the exception message.
    3. Create an reference of ListOfNumbers as list.
    4. Now invoke the writeList and print the result.

Pseudo code:

class ListOfNumbers {
    public int[] arrayOfNumbers = new int[10];
    
    public void writeList() {
        try {
            arrayOfNumbers[10] = 11;
        } catch (NumberFormatException e1) {
            System.out.println("NumberFormatException:" + " " + e1.getMessage());
        } catch (IndexOutOfBoundsException e2) {
            System.out.println("IndexOutOfBoundsException:" + " " + e2.getMessage());
        }
    }
        
    public static void main(String[] args) {
        ListOfNumber list = new ListOfNumber();
        list.writeList();
    }
}

*/

public class ListOfNumber {
    
    public int[] arrayOfNumbers = new int[10];
    
    public void writeList() {
        try {
            arrayOfNumbers[10] = 11;
            System.out.println("print the array: " + arrayOfNumbers);
        } catch (NumberFormatException e1) {
            System.out.println("NumberFormatException:" + " " + e1.getMessage());
        } catch (IndexOutOfBoundsException e2) {
            System.out.println("IndexOutOfBoundsException:" + " " + e2.getMessage());
        }
    }
        
    public static void main(String[] args) {
        ListOfNumber list = new ListOfNumber();
        list.writeList();
    }
}

