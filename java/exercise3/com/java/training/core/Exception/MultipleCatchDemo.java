package com.java.training.core.Exception;

/*
Requirement:
    To Demonstrate catching multiple exception.

Entity:
    public class MultipleCatchDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an integer array that stores 6 values.
    2. try to set the sixth element as 30/0 and print it.
    3. If any exceptions thrown by it catch that exception and print the exception message.
    
Pseudo code:
class MultipleCatchDemo {

       public static void main(String[] args) {
       try {    
           int[] array=new int[5];    
           array[5]=30/0;    
           System.out.println(array);  
           } catch (ArithmeticException e) { 
               System.out.println("Arithmetic Exception occurs");  
           } catch(ArrayIndexOutOfBoundsException e) { 
               System.out.println("ArrayIndexOutOfBounds Exception occurs");  
           } catch(Exception e) {
               System.out.println("Parent Exception occurs");  
           }             
           
        System.out.println("rest of the code");    
    }
}

*/

public class MultipleCatchDemo {
    
    public static void main(String[] args) {
        try {    
            int[] array=new int[5];    
            array[5]=30/0;    
            System.out.println(array);  
        } catch (ArithmeticException e) { 
              System.out.println("Arithmetic Exception occurs");  
        } catch(ArrayIndexOutOfBoundsException e) { 
              System.out.println("ArrayIndexOutOfBounds Exception occurs");  
        } catch(Exception e) {
              System.out.println("Parent Exception occurs");  
        }             
           
        System.out.println("rest of the code");    
    }
}
