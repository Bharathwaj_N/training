package com.java.training.core.Exception;

/*
Requirement:
    To demonstrate the difference between throw and throws.

Entity:
    public class ThrowThrowsDiffernceDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1.Demonstrate the example to establish the difference between throw and throws
    
Answer:
    Throw throws an exception explicitly and throws works as an try catch blocks. 
*/

public class ThrowThrowsDiffernceDemo {

    public void checkAge(int age) {
        
        // Throw keyword is used to throw an exception explicitly.
        if (age < 18) {
            throw new ArithmeticException("Not Eligible for voting");
        } else {
            System.out.println("Eligible for voting");
        }
    }

    // Throws is used to declare an exception, which means it works similar to the try-catch.
    public int division(int number1, int number2) throws ArithmeticException {
        int t = number1 / number2;
        return t;
    }

    public static void main(String args[]) {
        ThrowThrowsDiffernceDemo object = new ThrowThrowsDiffernceDemo();
        object.checkAge(13);
        System.out.println("End Of Program");
        
        ThrowThrowsDiffernceDemo object1 = new ThrowThrowsDiffernceDemo();
        try {
           System.out.println(object1.division(15,0));  
        }
        catch (ArithmeticException e) {
           System.out.println("You shouldn't divide number by zero");
        }
    }
}


