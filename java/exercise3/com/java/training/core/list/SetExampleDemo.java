package com.java.training.core.list;

/*
Requirement:
    To create a set and to add elements and to use addAll() and removeAll().
    
Entity:
    public class SetExampleDemo
    
Function Declaration:
    public static void main(String[] args)
    
jobs to be done:
    1. First create a set of type String whose object is named as string and add the elements to it
    2. Create an another string and add elements separately to it and print that set 
        2.1) Now add all the elements of string to string2 and print it.
    3. Iterate the set to print the elements by the given methods.
    
pseudo code:
public class SetExampleDemo {
    
    public static void main(String[] args) {
        Set<String> string = new HashSet<>();
        string.add("Cricket");
        string.add("Volleyball");
        string.add("Football");
        string.add("Kabadi");
        string.add("Hockey");
        string.add("Ice Skating");
        string.add("BasketBall");
        string.add("Throwball");
        string.add("Rugby");
        string.add("Tennis");
        System.out.println("The Set1 is " + " " + string);
        
        Set<String> string2 = new HashSet<>();
        string2.add("Dhoni");
        string2.add("Prabakaran");
        System.out.println("the second set is" + " " + string2);
        
        string2.addAll(string);
        System.out.println("the modified set is " + " " + string2);
        
        // to remove all the elements
        System.out.println(" the removed set is" + " " + string.removeAll(string2));
        
        // Using iterator
        Iterator<String> iterator = string.iterator();
        
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.println("using iterator " + " " + element);
        }
        
        // using for each loop
        for (String value : string) {
            System.out.println("for each loop" + " " + value);
        }
    }
}

*/

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetExampleDemo {
    
    public static void main(String[] args) {
        Set<String> string = new HashSet<>();
        string.add("Cricket");
        string.add("Volleyball");
        string.add("Football");
        string.add("Kabadi");
        string.add("Hockey");
        string.add("Ice Skating");
        string.add("BasketBall");
        string.add("Throwball");
        string.add("Rugby");
        string.add("Tennis");
        System.out.println("The Set1 is " + " " + string);
        
        Set<String> string2 = new HashSet<>();
        string2.add("Dhoni");
        string2.add("Prabakaran");
        System.out.println("the second set is" + " " + string2);
        
        string2.addAll(string);
        System.out.println("the modified set is " + " " + string2);
        
        // to remove all the elements
        System.out.println(" the removed set is" + " " + string.removeAll(string2));
        
        // Using iterator
        Iterator<String> iterator = string.iterator();
        
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.println("using iterator " + " " + element);
        }
        
        // using for each loop
        for (String value : string) {
            System.out.println("for each loop" + " " + value);
        }
    }
}
