package com.java.training.core.list;

/*
Requirement:
    To create a list and to add 10 elements 
    1. create another list and to use add all method
    2. Use indexOf() and lastIndexOf() methods
    3. To print the elements using for loops and iterator and stream
    4. To convert the list to string and array.

Entity:
    public class ListDemo
    
Function Declaration:
    public static void main(String[] args)
    indexOf(),lastIndex(), list.Stream(), hasNext(), next().

Jobs to be done:
    1. Create an object for the list and add elements to it
    2. Now create another list whose object is list1 and add elements to it also.
    3. Now add all the elements of list into list1.
    4. Find the first and last index of the element in the list.
    5. Convert the list into Set and array.
    6. Iterate the list and print the value using stream and for each loops.
    
Pseudo code:
public class ListDemo {
    
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        // Add the elements to the list.
        list.add("Black");
        list.add("White");
        list.add("Orange");
        list.add("Scorpian");
        list.add("Velarian");
        list.add("Indica");
        list.add("Vilot");
        list.add("Brown");
        list.add("Purple");
        list.add("Yellow");
        // Print the list.
        System.out.println("The list is" + " " + list);
        
        List<String> list1 = new ArrayList<>();
        list1.add("Red");
        list1.add("Green");
        list1.add("Blue");
        System.out.println("The Second list is" + " " + list1);
        
        // Now add all elements in list1 to list
        list.addAll(list1);
        
        // print the list
        System.out.println("The Modified list is " + " " + list);
        
        // Print the index of first element
        System.out.println("the first index is" + " " + list.indexOf("Orange"));
        
        // print the index of last element
        System.out.println("The last index is" + " " + list.lastIndexOf("purple"));
        
        // print the elements using enhanced for loop
        for (String value : list) {
            System.out.println("the elements are" + value);
        }
        
        // Creating an array for a list
        String[] colorArray = new String[list.size()];
        colorArray = list.toArray(colorArray);
        System.out.println("the list in array "+ " " + Arrays.toString(colorArray));
        
        // Converting List into an Set
        Set<String> alphaSet = new HashSet<>(list);
        System.out.println("The Set Values are" );
        
        for (String value1 : alphaSet) {
            System.out.println(value1);
        }
        
        // print the elements using for loop
        for (int integer = 0; integer < list1.size(); integer++) {
            System.out.println("The elements are" + " " + list1);
        }
        
        // Using Iterator
        Iterator<String> iteratorList = list.iterator();
        while (iteratorList.hasNext()) {
            System.out.println("the elements are" + " " + iteratorList.next());
        }
        
        // Using Stream
        list.stream().forEach ((element) -> System.out.println(element));
    }
}
*/

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

public class ListDemo {
    
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        // Add the elements to the list.
        list.add("Black");
        list.add("White");
        list.add("Orange");
        list.add("Scorpian");
        list.add("Velarian");
        list.add("Indica");
        list.add("Vilot");
        list.add("Brown");
        list.add("Purple");
        list.add("Yellow");
        // Print the list.
        System.out.println("The list is" + " " + list);
        
        List<String> list1 = new ArrayList<>();
        list1.add("Red");
        list1.add("Green");
        list1.add("Blue");
        System.out.println("The Second list is" + " " + list1);
        
        // Now add all elements in list1 to list
        list.addAll(list1);
        
        // print the list
        System.out.println("The Modified list is " + " " + list);
        
        // Print the index of first element
        System.out.println("the first index is" + " " + list.indexOf("Orange"));
        
        // print the index of last element
        System.out.println("The last index is" + " " + list.lastIndexOf("purple"));
        
        // print the elements using enhanced for loop
        for (String value : list) {
            System.out.println("the elements are" + value);
        }
        
        // Creating an array for a list
        String[] colorArray = new String[list.size()];
        colorArray = list.toArray(colorArray);
        System.out.println("the list in array "+ " " + Arrays.toString(colorArray));
        
        // Converting List into an Set
        Set<String> alphaSet = new HashSet<>(list);
        System.out.println("The Set Values are" );
        
        for (String value1 : alphaSet) {
            System.out.println(value1);
        }
        
        // print the elements using for loop
        for (int integer = 0; integer < list1.size(); integer++) {
            System.out.println("The elements are" + " " + list1);
        }
        
        // Using Iterator
        Iterator<String> iteratorList = list.iterator();
        while (iteratorList.hasNext()) {
            System.out.println("the elements are" + " " + iteratorList.next());
        }
        
        // Using Stream
        list.stream().forEach ((element) -> System.out.println(element));
    }
        
}


