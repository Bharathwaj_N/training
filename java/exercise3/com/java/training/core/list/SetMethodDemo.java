package com.java.training.core.list;

/*
Requirement:
    To describe about the contains() method and isEmpty() method.

Entity:
    public class SetExampleDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create a set of object setA and an elements to it.
    2. Check whether the setA contains the given element or not.
        2.1) Check and print the boolean value 
    3. Check and print the result in boolean whether the list is empty or not.
    
Pseudo code:
public class SetMethodDemo {
    
    public static void main(String[] args) {
        Set<String> setA = new HashSet<>();
        setA.add("Hero");
        setA.add("Heroien");
        setA.add("Director");
        setA.add("Editor");
        setA.add("Music Director");
        System.out.println("Does the Set setA contains Writer" + " " + setA.contains("Writer"));
        System.out.println("IS the Set is empty" + " " + setA.isEmpty());
        
    }

}

*/

import java.util.Set;
import java.util.HashSet;

public class SetMethodDemo {
    
    public static void main(String[] args) {
        Set<String> setA = new HashSet<>();
        setA.add("Hero");
        setA.add("Heroien");
        setA.add("Director");
        setA.add("Editor");
        setA.add("Music Director");
        System.out.println("Does the Set setA contains Writer" + " " + setA.contains("Writer"));
        System.out.println("IS the Set is empty" + " " + setA.isEmpty());
        
    }

}
