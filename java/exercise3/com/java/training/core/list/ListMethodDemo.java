package com.java.training.core.list;

/*
Requirement:
    To Explain about contains, retainAll() and subString

Entity:
    public class ListMethodDemo

function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an list of type integer whose object is integer.
    2. Add an elements to it 
    3. Check whether the list contains an element given
        3.1) if it contains then print "the value is present."
        3.2) else print "the value not present.
    4. Create an another list of object ddInteger and add elements to it.
        4.1) print all the elements common to both integer and oddInteger.
    5. Create a sublist for newly created list and print that sublist.
    
Pseudo code:
public class ListMethodDemo {
    
    public static void main(String[] args) {
        List<Integer> integer = new ArrayList<>();
        integer.add(10);
        integer.add(20);
        integer.add(30);
        
        // contains is used to check whether the value is present
        if (integer.contains(25) == true) {
            System.out.println("the value is present");
        } else {
            System.out.println("The value is not present");
        }
        
        // retainAll() method used to retain all the matching elements
        List<Integer> oddInteger = new ArrayList<>();
        oddInteger.add(30);
        oddInteger.add(20);
        oddInteger.add(55);
        integer.retainAll(oddInteger);
        System.out.println(" the result of retainAll method is" +  " " + integer);
        
        // Sublist is used to produce the desired values from the given indexes.
        List<String> string = new ArrayList<>();
        string.add("For");
        string.add("produce");
        string.add("exact");
        string.add("copy");
        string.add("material");
        System.out.println(" the List is" + string);
        
        // now create another list
        List<String> subString = new ArrayList<>();
        subString = string.subList(2, 4);
        System.out.println("The sublist is " + " " + subString);
    }

}
        
*/

import java.util.List;
import java.util.ArrayList;

public class ListMethodDemo {
    
    public static void main(String[] args) {
        List<Integer> integer = new ArrayList<>();
        integer.add(10);
        integer.add(20);
        integer.add(30);
        
        // contains is used to check whether the value is present
        if (integer.contains(25) == true) {
            System.out.println("the value is present");
        } else {
            System.out.println("The value is not present");
        }
        
        // retainAll() method used to retain all the matching elements
        List<Integer> oddInteger = new ArrayList<>();
        oddInteger.add(30);
        oddInteger.add(20);
        oddInteger.add(55);
        integer.retainAll(oddInteger);
        System.out.println(" the result of retainAll method is" +  " " + integer);
        
        // Sublist is used to produce the desired values from the given indexes.
        List<String> string = new ArrayList<>();
        string.add("For");
        string.add("produce");
        string.add("exact");
        string.add("copy");
        string.add("material");
        System.out.println(" the List is" + string);
        
        // now create another list
        List<String> subString = new ArrayList<>();
        subString = string.subList(2, 4);
        System.out.println("The sublist is " + " " + subString);
    }

}
