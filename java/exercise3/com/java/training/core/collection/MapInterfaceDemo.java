package com.java.training.core.collection;

/*
Requirement:
    To demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() )

Entity:
     public class MapInterfaceDemo

Function declaration:
     public static void main(String[] args) 

Jobs to be done:
     1) Create reference for TreeMap with key as Integer type and value as String type.
     2) Add some key-value pairs to the TreeMap reference..
     3) Print the TreeMap.
     4) Remove a element from the TreeMap.
     5) Print the TreeMap.
     6) Check whether the TreeMap has a value and print the result.
     7) Replace the value of a key.
     8) Print the treemap.
  
Pseudo code:
  
public class MapInterface {

    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        //Add the values 
        treeMap.remove(key value which have to remove);
        System.out.println("After removing element the map is " + treeMap);
        System.out.println(treeMap.containsValue(Value which have to remove));
        treeMap.replace(key,value);
        System.out.println("After replacing the key and value of map " + treeMap);
        }
  }
*/

import java.util.TreeMap;

public class MapInterfaceDemo {
    
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(10, "Red");
        treeMap.put(20, "Green");
        treeMap.put(30, "Blue");
        treeMap.put(40, "Black");
        treeMap.put(50, "White");
        System.out.println("the treemap is " + treeMap);
        
        treeMap.remove(20);
        System.out.println("The map after removed value is: " + treeMap);
        System.out.println(treeMap.containsValue("White"));
        
        treeMap.replace(30, "Purple");
        System.out.println("The replaced map is: " + treeMap);
    }
}

