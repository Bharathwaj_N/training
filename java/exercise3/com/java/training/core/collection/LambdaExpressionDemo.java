/*
Requirement:
    Addition,Subtraction,Multiplication and Division concepts are achieved using Lambda expression
    and functional interface

Entity:
    LambdaExample
    Arithmetic

Function declaration:
    int operation(int a, int b)

Jobs to be done:
    1.Add the two integer using lambda expression and print it.
    2.Subtract the two integer using lambda expression and print it.
    3.Multiply the two integer using lambda expression and print it.
    4.Divide the two integer using lambda expression and print it.

pseudo code:
interface Arithmetic {
    int operation(int a, int b);
}


public class LambdaExpressionDemo {
    public static void main(String[] args) {

        Arithmetic addition = (int a, int b) -> (a + b);
        System.out.println("Addition = " + addition.operation(5, 6));

        Arithmetic subtraction = (int a, int b) -> (a - b);
        System.out.println("Subtraction = " + subtraction.operation(5, 3));

        Arithmetic multiplication = (int a, int b) -> (a * b);
        System.out.println("Multiplication = " + multiplication.operation(4, 6));

        Arithmetic division = (int a, int b) -> (a / b);
        System.out.println("Division = " + division.operation(12, 6));

    }
}
*/

package com.java.training.core.collection;

interface Arithmetic {
	int operation(int a, int b);
}


public class LambdaExpressionDemo {
	public static void main(String[] args) {

		Arithmetic addition = (int a, int b) -> (a + b);
		System.out.println("Addition = " + addition.operation(5, 6));

		Arithmetic subtraction = (int a, int b) -> (a - b);
		System.out.println("Subtraction = " + subtraction.operation(5, 3));

		Arithmetic multiplication = (int a, int b) -> (a * b);
		System.out.println("Multiplication = " + multiplication.operation(4, 6));

		Arithmetic division = (int a, int b) -> (a / b);
		System.out.println("Division = " + division.operation(12, 6));

	}
}
