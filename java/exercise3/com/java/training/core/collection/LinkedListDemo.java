package com.java.training.core.collection;

/*
Requirement:
    To create an Array list of 7 elements and to create an empty linked list and to store the elements
    of array list to linked list and to display it.

Entity:
    LinkedListDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
   1. Create a ArrayList with type Integer
        1.1 Add the seven elements to the ArrayList
   2. Print the  ArrayList.
   3. Create a reference LinkedList With type as Integer.
        3.1 Add all the elements from ArrayList to LinkedList.
   4. Print the LinkedList.

pseudo code:
class LinkedListDemo {

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        //Add the seven elements to the ArrayList
        System.out.println("Array list elements are " + arrayList);
            
        //Create the LinkedList
        LinkedList<Integer> linkedList = new LinkedList<>();
        
        //add all the elements from arrayList to LinkedList
        linkedList.addAll(arrayList);
          
        //Using forEach loop
        System.out.println("Linkedlist elements are using forEach");
        for (int elements : linkedList) {
            System.out.print(elements + " ");
        }
        System.out.println();
    }

}
*/

import java.util.ArrayList;
import java.util.LinkedList;

public class LinkedListDemo {
    
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(115);
        list.add(278);
        list.add(546);
        list.add(776);
        list.add(1000);
        list.add(768);
        list.add(458);
        
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.addAll(list);
        
        for (int numbers : linkedList) {
            System.out.print(numbers + " ");
        }
    }
}
