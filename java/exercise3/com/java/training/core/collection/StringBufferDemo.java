/*
Requirements:
    To demonstrate insertions and string buffer in tree set. 
     
Entities:
    public class StringBufferDemo 
      
Function Declaration:
    public static void main(String[] args)
      
Jobs To Done:
    1) Create treeset reference with Stringbuffer.
       2) Add the values in treeset.
       3) print the treeset.
       4) Create method to compare two string.
 
PseudoCode:
public class StringBufferDemo {
   
    public static void main(String[] args) {
        TreeSet<StringBuffer> treeset = new TreeSet<StringBuffer>(new StringComp());
        //Add the values in the treeset
        System.out.println(treeset);
    }
}
    class StringCompare implements Comparator<StringBuffer> {
        public int compare(StringBuffer string1, StringBuffer string2) {
            //compare the two strings and return it.
        }
     }
*/

package com.java.training.core.collection;

import java.util.Comparator;
import java.util.TreeSet;

public class StringBufferDemo {

	public static void main(String[] args) {
		TreeSet<StringBuffer> treeSet = new TreeSet<>();
		treeSet.add(new StringBuffer("A"));
		treeSet.add(new StringBuffer("B"));
		treeSet.add(new StringBuffer("C"));
		treeSet.add(new StringBuffer("D"));
		treeSet.add(new StringBuffer("E"));
		System.out.println(treeSet);
	}
}

class StringCompare implements Comparator<StringBuffer> {
    
    public int compare(StringBuffer string1, StringBuffer string2) {
	    String value1 = string1.toString();
		String value2 = string2.toString();
		return value1.compareTo(value2);
	}
	
}


/*
Explanation:
String class and all the Wrapper classes already implements Comparable interface but StringBuffer 
class doesn�t implements Comparable interface. Hence, we get a ClassCastException in the above 
example.
*/