package com.java.training.core.collection;

import java.util.Stack;

/*
Requirement:
    Add and remove the elements in stack.      
Entity:
    StackDemo

Method Signature:
    public static void main(String[] args)

Jobs To Be Done:
    1) Create reference to stack.
    2) Add the values to Stack.
    3) Print the stack .
    4) Remove some of the stack elements.
    5) Print the stack .
   
Pseudo code:
 public class StackDemo {

    public static void main(String[] args) {
        Stack<Integer> sample = new Stack<>();
        //Add the values in the stack
         * System.out.println(sample);
        //remove element from the stack
        System.out.println("After removing the some of element :" + sample);
        }
    }
 
*/

public class StackDemo {
    
    public static void main(String[] args) {
        Stack<Integer> sample = new Stack<>();
        sample.push(22);
        sample.push(44);
        sample.push(56);
        sample.push(45);
        sample.push(34);
        sample.push(23);
        sample.push(109);
        System.out.println(sample);
        
        sample.remove(3);
        sample.remove(4);
        System.out.println("After removing some element :" + " " + sample);
    }

}
