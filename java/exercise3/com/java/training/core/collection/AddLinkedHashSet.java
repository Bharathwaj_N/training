package com.java.training.core.collection;

/*
Requirements:
    To demonstrate the basic add and traversal operation of linked hash set.
  
Entities:
    AddHashSet 
 
Function Declaration:
    public static void main(String[] args)
     
Jobs To Be Done:
    1.Create a reference for LinkedHashSet with type as Integer.
    2.Add the elements to the Set.
    3.Print the elements of LinkedHashSet.
    4.Create a reference for iterator with type as Integer.
    5.for each element in the LinkedHashSet
         5.1) Print the elements.
    
Pseudo code:
public class AddLinkedHashSet {

    public static void main(String[] args) {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        //add the set elements.
        System.out.println("Set elements are " + set);
        
        System.out.println("Set elements are using iterator");
        Iterator<Integer> iterator = set.iterator();
        
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        
        System.out.println();
    }
}

*/

import java.util.Iterator;
import java.util.LinkedHashSet;

public class AddLinkedHashSet {

	public static void main(String[] args) {
		LinkedHashSet<Integer> set = new LinkedHashSet<>();
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(4);
		set.add(5);
		System.out.println("Set elements are " + set);
		
		System.out.println("Set elements are using iterator");
		Iterator<Integer> iterator = set.iterator();
		
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		
		System.out.println();
	}
}
