package com.java.training.core.collection;

/*
Requirement:
    To demonstrate the difference between poll() method and remove() method.

Entity:
    RemoveAndPoll

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1) Create a reference for queue with Integer type.
    2) Add the elements in the queue.
    3) remove the elements by using poll.
    4) print the queue.
    5) Clear the queue.
    6) Print the result of invoking the poll method.
    7) Create a reference for queue with String type.
    8) Add the elements in the queue.
    9) remove the elements by using remove method.
    10) print the queue.
    11) Clear the queue.
    12) Print the result of invoking the poll method.

Pseudo code:
public class RemoveAndPoll {

    public static void main(String[] args) {
        Queue<Integer> queue = new PriorityQueue<>();
        //Add the elements 
        System.out.println("Before using poll :" + queue);
        queue.poll();
        System.out.println("After using poll :" + queue);
        queue.clear();
        System.out.println(queue.poll());
        
        Queue<String> string = new PriorityQueue<>();
        //add the elements
        System.out.println("Before using remove :" + string);
        string.remove();
        System.out.println("After using remove :" + string);
        string.clear();
        System.out.println(queue.remove());
        }
  }
*/
import java.util.Queue;
import java.util.PriorityQueue;
public class RemoveAndPoll {
    
    public static void main(String[] args) {
        Queue<Integer> queue = new PriorityQueue<>();
        queue.add(555);
        queue.add(556);
        queue.add(557);
        queue.add(558);
        queue.add(559);
        queue.poll();
        System.out.println("After poll " + queue);
        
        queue.clear();
        System.out.println("After clear " + queue.poll());  // prints null.
        
        Queue<String> string = new PriorityQueue<>();
        string.add("Jackson");
        string.add("Ema Watson");
        string.add("He was assasinated");
        string.add("Come and get your Material");
        string.add("Thiis is my program");
        string.add("Mahesh");
        string.remove();
        System.out.println("After remove method " + string);
        
        string.clear();
        System.out.println("After the clear method " + string.remove()); // throws exception
    }
}

/* 
From this example the poll() is used to remove or retrieve the top element and when the queue is empty
it returns the null but remove() throws exception.
*/