package com.java.training.core.collection;

import java.util.Collections;
import java.util.Vector;

/*
Requirement:
    To code for sorting the vector in the descending order.

Entity:
    VectorDemo

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1. Create an object for Vector of Integer type and add the elements to it.
    2. Now by using collections and sort method sort the elements
    3. Print the sorted vector. 
    
Pseudo Code:
class VectorDemo {
    
    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<>();
        // Add the elements of vector
        // print the vector
        
        Collections.sort(vector, Collections.reverseOrder());
        System.out.println(vector);
    }
}
*/

public class VectorDemo {
    
    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<>();
        vector.add(115);
        vector.add(455);
        vector.add(121);
        vector.add(400);
        vector.add(768);
        vector.add(676);
        vector.add(556);
        System.out.println("Vector before sorting " + vector);
        
        Collections.sort(vector, Collections.reverseOrder());
        System.out.println("The vector after sorting in descending order is " + vector);
    }

}

/*Her is a **** */
/* T//m*/
// /* //// */

