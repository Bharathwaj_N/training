package com.java.training.core.collection;

import java.util.ArrayDeque;

/*
Requirement:
    To use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() 
    methods to store and retrieve elements in ArrayDequeue.
    
Entity:
    ArrayDequeDemo
    
Method Signature:
    public static void main(String[] args)

Jobs to be Done:
    1.Create a reference for ArrayDeque with Integer type.
    2.Add all the elements to the ArrayDeque.
    3.Add another elements to the first and last index of the ArrayDeque.
    4.Print the elements of ArrayDeque.
    5.Remove the first and last index of the elements from ArrayDeque and print the ArrayDeque.
    7.Print the first and last peek element in the ArrayDeque.
    8.Print the first and last poll element in the ArrayDeque.
    
 Pseudo code:
 class ArrayDequeDemo {

    public static void main(String[] args) {
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        //Add all the elements to the ArrayDeque
         
        System.out.println("The ArrayDeque " + deque);

        // Add first element to the ArrayDeque
        deque.addFirst(element);
        System.out.println("After adding the first element" + deque);
        
        //Add last element to the ArrayDeque
        deque.addLast(element);
        System.out.println("After adding the last element" + deque); 
        
        //Remove the first element to the ArrayDeque
        deque.removeFirst();
        System.out.println("After removing the first element" + deque);
        
        //Remove the last element to the ArrayDeque
        deque.removeLast();
        System.out.println("After removing the Last element" + deque);
 
        System.out.println("The first peek element " + deque.peekFirst());
        System.out.println("The last  peek element " + deque.peekLast());
        System.out.println("The first poll element " + deque.pollFirst());
        System.out.println("The last poll element " + deque.pollLast());
        

    }

  }
*/

public class ArrayDequeDemo {
    
    public static void main(String[] args) {
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        deque.add(1);
        deque.add(2);
        deque.add(3);
        deque.add(4);
        deque.add(5);
        deque.add(6);
        deque.add(7);
        deque.add(8);
        System.out.println("The ArrayDeque " + deque);
        
        deque.addFirst(123);
        System.out.println("After adding the first element" + deque);
        
        deque.addLast(987);
        System.out.println("After adding the last element" + deque);
        
        deque.removeFirst();
        System.out.println("After removing the first element" + deque);
        
        deque.removeLast();
        System.out.println("After removing the last element" + deque);
        
        System.out.println("The first peek element " + deque.peekFirst());
        System.out.println("The last  peek element " + deque.peekLast());
        
        System.out.println("The first poll element " + deque.pollFirst());
        System.out.println("The last poll element " + deque.pollLast());

    }

}
