package com.java.training.core.collection;

/*
Requirement:
    8 districts are shown belowMadurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy
    to be converted to UPPERCASE.
Entity:
    Districts
Function Declaration:
    public static void main(String[] args)
jobs to be done:
    1. Create a List with type String and add the given values in it
    2. List values are replace all to UpperCase
    3. Print the list.

pseudo code:
public class Districts {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur",
                "Salem", "Erode", "Trichy");
        list.replaceAll(String::toUpperCase);
        System.out.println(list);
    }

}

*/


import java.util.Arrays;
import java.util.List;

public class Districts {

	public static void main(String[] args) {
		List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur",
				"Salem", "Erode", "Trichy");
		list.replaceAll(String::toUpperCase);
		System.out.println(list);
	}

}
