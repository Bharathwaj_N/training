package com.java.training.core.collection;

/*
Requirements:
    To demonstrate the basic add and traversal operation of linked hash set.
  
Entities:
    public class HashSetDemo
 
Function Declaration:
    public static void main(String[] args)
     
Jobs To Be Done:
   1. Create a reference for HashSet with type Integer.
         1.1 Add  the elements to the Set.
   2. Print the elements in the HashSet.
   3. Remove the element from the HashSet.
   4. Print the elements in the HashSet.
   5. for each element in the set
         5.1.Print the element

Pseudo code:
        public static void main(String[] args) {
            LinkedHashSet<Integer> set = new LinkedHashSet<>();
            //Add the elements to the Set 
        
            System.out.println("Set elements are " + set);
        
            //Using iterator
            System.out.println("Set elements are using iterator");
            for (int element : set) {
               System.out.println(element + " ");
            } 
        }
    }
*/


import java.util.HashSet;

public class HashSetDemo {
    
    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        set.add(115);
        set.add(220);
        set.add(223);
        set.add(222);
        set.add(226);
        System.out.println("Displaying elements in set " + set);
        
        set.remove(222);
        System.out.println("Printing the set after removing the elemnt " + set);
        
        System.out.println("Displaying element using iterating method ");
        
        for (int element : set) {
            System.out.println(element + " ");
        }
    }

}
