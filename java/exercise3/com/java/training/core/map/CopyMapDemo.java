package com.java.training.core.map;

/*
Requirement:
    To copy the elements of one map to an another.

Entity:
    public class CopyMapDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an objects hashMap1 and hashMap2 and add elements to it.
    2. Copy the elements of hashmap1 to hashMap2 and print the result.
    
Pseudo code:
public class CopyMapDemo {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap1 = new HashMap<>();
        hashMap1.put(1, "Red");
        hashMap1.put(2, "Green");
        hashMap1.put(3, "Black");
        System.out.println("The values of hashMap1 is" + " " + hashMap1);
        
        HashMap<Integer, String> hashMap2 = new HashMap<>();
        hashMap2.put(4, "White");
        hashMap2.put(6, "Blue");
        hashMap2.put(5, "Orange");
        System.out.println("the value of hashMap2 is" + " " + hashMap2);
        
        hashMap2.putAll(hashMap1);
        System.out.println("Now the value of hashMap2 is" + " " + hashMap2);
    }

}
    
*/

import java.util.HashMap;

public class CopyMapDemo {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap1 = new HashMap<>();
        hashMap1.put(1, "Red");
        hashMap1.put(2, "Green");
        hashMap1.put(3, "Black");
        System.out.println("The values of hashMap1 is" + " " + hashMap1);
        
        HashMap<Integer, String> hashMap2 = new HashMap<>();
        hashMap2.put(4, "White");
        hashMap2.put(6, "Blue");
        hashMap2.put(5, "Orange");
        System.out.println("the value of hashMap2 is" + " " + hashMap2);
        
        hashMap2.putAll(hashMap1);
        System.out.println("Now the value of hashMap2 is" + " " + hashMap2);
    }

}
