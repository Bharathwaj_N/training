package com.java.training.core.map;

/*
Requirement:
    To write a Java program to test if a map contains a mapping for the specified key.

Entity:
    public class ContainsMapDemo.
    
Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1. Create an object for HashMap hashMap of key of type Integer and value of type String and add the elements.
    2. Check and print the boolean value if it contains the given value.
    
pseudo code:
public class ContainsMapDemo {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(10, "Actor");
        hashMap.put(25, "Vijay");
        hashMap.put(20, "Vijay");
        hashMap.put(76, "Vijay");
        hashMap.put(12, "Vijay");
        boolean value = hashMap.containsKey(11);
        System.out.println("if the key 11 presents in hashMap" + " " + value);
        
        boolean value1 = hashMap.containsKey(20);
        System.out.println("if the key 20 presents in hashMap" + " " + value1);
    }

}
*/

import java.util.HashMap;

public class ContainsMapDemo {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(10, "Actor");
        hashMap.put(25, "Vijay");
        hashMap.put(20, "Vijay");
        hashMap.put(76, "Vijay");
        hashMap.put(12, "Vijay");
        boolean value = hashMap.containsKey(11);
        System.out.println("if the key 11 presents in hashMap" + " " + value);
        
        boolean value1 = hashMap.containsKey(20);
        System.out.println("if the key 20 presents in hashMap" + " " + value1);
    }

}
