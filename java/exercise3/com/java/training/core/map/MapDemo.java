package com.java.training.core.map;

/*
Requirement:
    Write a Java program to get the portion of a map whose keys range from a given key to another key.
     
Entity:
    public class MapDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1. Create two maps TreeMap and SortedMap whose objects are treeMap and subTreeMap respectively.
    2. Now add the elements to treeMap and take a portion of treeMap and copy it to subTreeMap.
    3. print the subTreeMap
    
Pseudo code:
public class MapDemo {
    
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        SortedMap<Integer, String> subTreeMap = new TreeMap<>();
        treeMap.put(10, "Red");
        treeMap.put(20, "Green");
        treeMap.put(30, "Blue");
        treeMap.put(40, "Black");
        treeMap.put(50, "White");
        System.out.println("the treemap is " + treeMap);
        subTreeMap = treeMap.subMap(20, 40);
        System.out.println("the range is " + subTreeMap);
    }

}

*/

import java.util.SortedMap;
import java.util.TreeMap;

public class MapDemo {
    
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        SortedMap<Integer, String> subTreeMap = new TreeMap<>();
        treeMap.put(10, "Red");
        treeMap.put(20, "Green");
        treeMap.put(30, "Blue");
        treeMap.put(40, "Black");
        treeMap.put(50, "White");
        System.out.println("the treemap is " + treeMap);
        subTreeMap = treeMap.subMap(20, 40);
        System.out.println("the range is " + subTreeMap);
    }

}
