package com.java.training.core.map;

import java.util.HashMap;

/*
Requirement:
    To write the java program to count size of mappings.

Entity:
    public class SizemappingDemo

Function Declaration:
    public static void main(String[] args)
    size()

Jobs to be done:
    1. Create an object of HashMap as hashMap and add the elements to it.
    2.print the size of the hashMap if it contains the elements.
    
Pseudo code:
public class SizeMappingDemo {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Bharathwaj");
        hashMap.put(2, "Ajay");
        hashMap.put(3, "Aniruth");
        hashMap.put(4, "Vijay");
        hashMap.put(5, "Sethupathy");
        hashMap.put(6, "Lawrance");
        hashMap.put(8, "Sugan");
        hashMap.put(7, "Ilan");
        System.out.println("the keys and values are " + hashMap);
        System.out.println("the count of the sizre of the mappings are " + hashMap.size());
    }

}

*/

public class SizeMappingDemo {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Bharathwaj");
        hashMap.put(2, "Ajay");
        hashMap.put(3, "Aniruth");
        hashMap.put(4, "Vijay");
        hashMap.put(5, "Sethupathy");
        hashMap.put(6, "Lawrance");
        hashMap.put(8, "Sugan");
        hashMap.put(7, "Ilan");
        System.out.println("the keys and values are " + hashMap);
        System.out.println("the count of the sizre of the mappings are " + hashMap.size());
    }

}
