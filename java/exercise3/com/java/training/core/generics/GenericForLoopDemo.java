package com.java.training.core.generics;

/*
Requirement:
    To demonstrate a program for generic for loop.

Entity:
    public class GenericForLoopExample

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Declare the class GenericForLoopDemo
    2. Under a main method create an object set for Set list for ArrayList and hashMap for HashMap.
    3. Using for loop iterate it and print the result.
    
PSeudo code:
public class GenericForLoopDemo {
    
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("Zero");
        set.add("Jackiechan");
        set.add("Robert");
        for (String string : set) {
            System.out.println("The elements are " + string);
        }
    }
}
*/

import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;

public class GenericForLoopDemo {
    
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("Zero");
        set.add("Jackiechan");
        set.add("Robert");
        for (String string : set) {
            System.out.println("The elements are " + string);
        }
        
        ArrayList<Integer> list = new ArrayList<>();
        list.add(125);
        list.add(150);
        list.add(175);
        for (int numbers : list) {
            System.out.println("the elements of the list are " + numbers);
        }
        
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Robert");
        hashMap.put(2, "Downey");
        hashMap.put(3, "Suresh");
        for (Integer key : hashMap.keySet()) {
            String value = hashMap.get(key);
            System.out.println(key + ":" + value);
        }
        
        for (String value : hashMap.values()) {
            System.out.println("The values are" + value);
        }
    }
}
