package com.java.training.core.generics;

/*
Requirements : 
    Write a generic method to count the number of elements in a collection that have a specific
    property (for example, odd integers, prime numbers, palindromes).

Entities :
    public class CountSpecificProperty.
  
Function Declaration :
    public static void main(String[] args)
  
Jobs To Be Done:
    1) Create a ArrayList reference with Integer type.
    2) Add 5 integers to the list.
    3) Create a method with Arraylist as arguement.
          3.1)Initialize a integer variable with 0 as its value.
          3.2)Iterate through the list and check each integer is odd.
               3.2.1)If the integer is odd then the variable is incremented.
          3.3)Return the variable.
    4)Print the number of odd numbers in the list using the method.
    
Pseudo code:
public class GenericMethodDemo {

    public static int countOddNumber(ArrayList<Integer> list) {
        int value = 0;
        
        for (int elements : list) {
            if (elements % 2 != 0) {
                value++;
            }
        }
        
        return value;
    }
    
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        // add the elements here
        // and call the method and print the result.
*/

import java.util.ArrayList;

public class GenericMethodDemo {
    
    public static  int countOddNumbers(ArrayList<Integer> list) {
        int value = 0;
        
        for (int elements : list) {
            if (elements % 2 != 0) {
                value++;
            }
        }
        
        return value;
    }
    
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(25);
        list.add(15);
        list.add(22);
        list.add(24);
        list.add(26);
        list.add(27);
        list.add(21);
        list.add(20);
        System.out.println("the count of odd number is " + countOddNumbers(list));
    }
}
