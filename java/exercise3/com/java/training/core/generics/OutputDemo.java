package com.java.training.core.generics;

/*
Requirement:
    To find the output of the below program.
    public class UseGenerics {
        public static void main(String[] args) {  
            MyGen<Integer> m = new MyGen<Integer>();  
            m.set("merit");
            System.out.println(m.get());
        }
    }
    class MyGen<T> {
    
        T var;
    
        void  set(T var) {
            this.var = var;
        }
        T get() {
            return var;
        }
    }
    
Entity:
    public class OutputDemo

Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Look at the program and find the output.
*/

public class OutputDemo {
    
    public static void main(String[] args) {  
        MyGen<Integer> m = new MyGen<Integer>();  
        //m.set("merit");    // compile time error
        System.out.println(m.get());
    }
}
class MyGen<T> {

    T var;

    void  set(T var) {
        this.var = var;
    }
    T get() {
        return var;
    }
}

// There will be compile time error 
