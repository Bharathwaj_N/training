package com.java.training.core.lambda;

/* 
Requirement:
    To print the volume of the rectangle using Lambda Expressions.
    
Entity:
    public class VolumeOfrectangle.

Function Declaration:
    public double printVolume(double length, double width, double height)
    public static void main(String[] args)

Jobs to be done:
    1)Get the input from user as length and breadth.
    2)Multiple the length and breadth using lambda expression.
    3)Print the rectangle area
 
Pseudo Code:
interface Rectangle {
    
    public double printVolume(double length, double width, double height);
}

public class VolumeOfRectangle {
    
    public static void main(String[] args) {
        Rectangle volume = (length, width, height) -> {
            double volumeOfRectangle = length * width * height;
            return (volumeOfRectangle);
        };
        
        System.out.println("The volume of the rectangle is: " + volume.printVolume(12.44, 667.434, 66.987));
    }
}
   
*/

interface Rectangle {
    
    public double printVolume(double length, double width, double height);
}

public class VolumeOfRectangle {
    
    public static void main(String[] args) {
        Rectangle volume = (length, width, height) -> {
            double volumeOfRectangle = length * width * height;
            return (volumeOfRectangle);
        };
        
        System.out.println("The volume of the rectangle is: " + volume.printVolume(12.44, 667.434, 66.987));
    }
}
