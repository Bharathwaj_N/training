package com.java.training.core.lambda;

/*
Requirement:
    To find the error line and to fix it using BiFunction Interface for the program.
    public interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
        return number1 + number2;
        };
        
        int print = function.print(int 23,int 32);
        
        System.out.println(print);
    }
}

Entity:
    public class TypeInterfaceDemo
    
Jobs to be done:
    1. To find the Wrong and to fix it.
*/

import java.util.function.BiFunction;

/*interface BiFunction {
    int print(int number1, int number2);
}*/

public class TypeInterfaceDemo {
    
    public static void main(String[] args) {

        BiFunction<Integer, Integer, Integer> function = (number1, number2) ->  { 
            return number1 + number2;
        };        
        
        System.out.println(function.apply(23, 32));
    }

}
