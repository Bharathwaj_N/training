package com.java.training.core.lambda;

/*
Requirement:
    To write a program to print difference of two numbers using lambda expression and the single method interface

Entity:
    public class DifferenceOfNumber

Function Declaration:
    public int printDifference(int firstNumber, int secondNumber)
    public static void main(String[] args)

Jobs to be done:
    1)Create Scanner object.
    2)Initialize two integer variables and assign the values by getting input from user.
    3)Define a lambda expression to return the difference between two numbers.
    4)Print the resultant difference.
    
Pseudo code:
interface NumberDifference {
    
    public int printDifference(int firstNumber, int secondNumber);
}

public class DifferenceOfNumber {
    
    public static void main(String[] args) {
        NumberDifference difference = (firstNumber, secondNumber) -> (secondNumber - firstNumber);
        System.out.println(difference.printDifference(112, 223));
    }

}
    
*/

interface NumberDifference {
    
    public int printDifference(int firstNumber, int secondNumber);
}

public class DifferenceOfNumber {
    
    public static void main(String[] args) {
        NumberDifference difference = (firstNumber, secondNumber) -> (secondNumber - firstNumber);
        System.out.println(difference.printDifference(112, 223));
    }

}
