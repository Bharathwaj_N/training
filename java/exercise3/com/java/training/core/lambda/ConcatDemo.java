package com.java.training.core.lambda;

/*
Requirement:
    To write a Lambda expression program with a single method interface to concatenate two strings.

Entity:
    public class ConCatDemo.
    interface ConCationDemo.

Function Declaration:
    public String conCat(String firstString, String secondString)
    public static void main(String[] args)

Jobs to be done:
    1)Define a lambda expression to return the number 5.
    2)Print the returned value.

Pseudo Code:
    interface ConcationDemo {
  
      public String concat(String firstString, String secondString);
  }
  class ConcatenateTwoStrings {
  
      public  static void main(String[] args) {
           ConcationDemo conCatString = (firstString, secondString) -> (firstString + secondString);
           System.out.println(conCatString.conCat("Vijay", "Ram"));
      }
  }

*/

interface ConcationDemo {
    
    public String conCat(String firstString, String secondString);
}

public class ConcatDemo {
    
    public static void main(String[] args) {
        ConcationDemo conCatString = (firstString, secondString) -> (firstString + secondString);
        System.out.println(conCatString.conCat("Vijay", "Ram"));
    }
}
