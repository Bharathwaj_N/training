package com.java.training.core.lambda;

/*Method references are effectively a subset of lambda expressions, because if a lambda expression can be used, then it might be possible to use a method reference.
 * TYPES: 
 * 		Reference to a static method
 * 		Reference to an instance method of a particular object
 * 		Reference to an instance method of an arbitrary object of a particular type
 * 		Reference to a constructor
*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

public class MethodReference {

	// Reference to a Static Method

	public static class StaticMethodReference {
		
		public static void main(String[] args) {
			List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
			// Method reference
			list.forEach(StaticMethodReference::print);
			// Lambda expression
			list.forEach(number -> StaticMethodReference.print(number));
			// normal
			for (int number : list) {
				StaticMethodReference.print(number);
			}
		}

		public static void print(final int number) {
			System.out.println("I am printing: " + number);
		}
	}

	// Reference to an Instance Method of a Particular Object

	public static class ParticularInstanceMethodReference {
		
		public static void main(String[] args) {
			final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
			final MyComparator myComparator = new MyComparator();
			// Method reference
			Collections.sort(list, myComparator::compare);
			// Lambda expression
			Collections.sort(list, (a, b) -> myComparator.compare(a, b));
		}

		private static class MyComparator {
			
			public int compare(final Integer a, final Integer b) {
				System.out.println(a.compareTo(b));
				return a.compareTo(b);
			}
		}
	}

	// Reference to an Instance Method of an Arbitrary Object of a Particular Type

	public static class ArbitraryInstanceMethodReference {
		
		public static void main(String[] args) {
			final List<Person> people = Arrays.asList(new Person("dan"), new Person("laura"));
			// Method reference
			people.forEach(Person::printName);
			// Lambda expression
			people.forEach(person -> person.printName());
			// normal
			for (final Person person : people) {
				person.printName();
			}
		}

		private static class Person {
			
			private String name;

			public Person(final String name) {
				this.name = name;
			}

			public void printName() {
				System.out.println(name);
			}
		}
	}

	// Reference to a Constructor

	public static class ConstructorMethodReference {
		
		public static void main(String[] args) {
			final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
			// Method Reference
			copyElements(null, ArrayList<Integer>::new);
			// Lambda expression
			copyElements(list, () -> new ArrayList<Integer>());
		}

		private static void copyElements(final List<Integer> list,
				final Supplier<Collection<Integer>> targetCollection) {
			// Method reference to a particular instance
			list.forEach(targetCollection.get()::add);
			System.out.println(list);
		}
	}
}
