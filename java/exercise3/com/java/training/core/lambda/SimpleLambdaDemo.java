package com.java.training.core.lambda;

/*
Requirement:
    To code the simple lambda expression using the method getValue()

Entity:
    public class SimpleLambdaDemo 

Function Declaration:
    public int getValue()
    public static void main(String[] args)
    
Jobs to be done:
    1) Define a lambda expression to return the number 555.
    2) Print the returned value.
    
Pseudo code:
interface LambdaDemo {
    
    public int getValue();
}

public class SimpleLambdaDemo {
    
    public static void main(String[] args) {
        LambdaDemo value = () -> 555;
        System.out.println("The value is: " + value.getValue());
    }
}

*/

interface LambdaDemo {
    
    public int getValue();
}

public class SimpleLambdaDemo {
    
    public static void main(String[] args) {
        LambdaDemo value = () -> 555;
        System.out.println("The value is: " + value.getValue());
    }
}
