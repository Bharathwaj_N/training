package com.java.training.stack;

/*
Requirement:
    To complete the following code and to find its output.
     Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());   

Entity:
    PriorityQueueDemo.

Function Declaration:
    public static void main(String[] args)
    poll(), peak().

Jobs to be Done:
    1. Create an object bike for queue and add the elements to queue.
    2. Print the peek element.
    
Pseudo code:
public class PriorityQueueDemo {
    
    public static void main(String[] args) {
        Queue<String> bike = new PriorityQueue<>();
        bike.add("Shine");
        bike.add("CBR 120");
        bike.add("FZ");
        bike.add("R15");
        bike.poll();
        System.out.println(bike.peek());
    }

}
*/

import java.util.*;

public class PriorityQueueDemo {
    
    public static void main(String[] args) {
        Queue<String> bike = new PriorityQueue<>();
        bike.add("Shine");
        bike.add("CBR 120");
        bike.add("FZ");
        bike.add("R15");
        bike.poll();
        System.out.println(bike.peek());
    }

}
