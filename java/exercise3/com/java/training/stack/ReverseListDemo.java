package com.java.training.stack;

/*
Requirement: 
    To reverse List Using Stack with minimum 7 elements in list.
  
Entity: 
    public class ReverseListDemo.
  
Function Declaration: 
    public static void main(String[] args)
  
Jobs To Be Done: 
    1) Create a ArrayList of object named integer and add element to it.
    2) Create a Stack of object stack.
    3) Fetch every elemnts from the List and iterate it over a size of the stack and remove the first element and print
       the result.
       
PSeudo code:

public class ReverseListDemo {

    public static void main(String[] args) {
        ArrayList<Integer> integer = new ArrayList<>();
        integer.add(55);
        integer.add(70);
        integer.add(98);
        integer.add(100);
        integer.add(139);
        integer.add(25);
        integer.add(12);
        integer.add(200);
        System.out.println("Before reversing the list" + integer);

        Stack<Integer> stack = new Stack<>();

        for (Integer number : integer) {
            stack.push(number);
        }

        integer.clear();
        int size = stack.size();

        for (int iteration = 0; iteration < size; iteration++) {
            integer.add(stack.pop());
        }

        System.out.println("After reversing the list" + integer);
    }
}

*/

import java.util.ArrayList;
import java.util.Stack;

public class ReverseListDemo {

    public static void main(String[] args) {
        ArrayList<Integer> integer = new ArrayList<>();
        integer.add(55);
        integer.add(70);
        integer.add(98);
        integer.add(100);
        integer.add(139);
        integer.add(25);
        integer.add(12);
        integer.add(200);
        System.out.println("Before reversing the list" + integer);

        Stack<Integer> stack = new Stack<>();

        for (Integer number : integer) {
            stack.push(number);
        }

        integer.clear();
        int size = stack.size();

        for (int iteration = 0; iteration < size; iteration++) {
            integer.add(stack.pop());
        }

        System.out.println("After reversing the list" + integer);
    }
}


