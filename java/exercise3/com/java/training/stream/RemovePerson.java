package com.java.training.stream;

/*
Requirements:
    List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
* Remove the following person from the roster List:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
 
 Entities:
    RemovePerson
    
 Function Signature:
    public static void main(String[] args) 
    
 Jobs To Be Done:
    1)Invoke a method createRoster from the class Person and store it to the roster List.
    2)Declare  a List and create reference for the list .
      2.1) Add the given person details to the newRoster list .
    3)For each elements in newRoster list .
      3.1)Print the elements .
    4)Remove the person "Bob" in roaster list .
    5)For each elements in roster list.
      5.1)Print the elements .
    
    
 Pseudo code:
class RemovePerson {
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        //Add all the person details
        Stream<Person> stream = newRoster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
        //remove the person Bob from roster.
        Stream<Person> stream1 = roster.stream();
        stream1.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));

    }

}
*/

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RemovePerson {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		List<Person> newRoster = new ArrayList<>();
		newRoster.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE,
				"john@example.com"));
		newRoster.add(new Person("Jade", IsoChronology.INSTANCE.date(1990, 7, 15),
				Person.Sex.FEMALE, "jade@example.com"));
		newRoster.add(new Person("Donald", IsoChronology.INSTANCE.date(1991, 8, 13),
				Person.Sex.MALE, "donald@example.com"));
		newRoster.add(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE,
				"bob@example.com"));
		Stream<Person> stream = newRoster.stream();
		stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
				+ person.gender + " " + person.emailAddress));
		System.out.println(" ");
		roster = roster.stream().filter(element -> element.getName() != "Bob")
				.collect(Collectors.toList());
		Stream<Person> stream1 = roster.stream();
		stream1.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
				+ person.gender + " " + person.emailAddress));

	}

}
