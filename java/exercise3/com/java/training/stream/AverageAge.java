package com.java.training.stream;

/*
Requirement:
    Write a program to find the average age of all the Person in the person List
Entity:
    AverageAge
Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an reference roster for the List of type person and assign a method createRoster to it.
    2. By using stream() make the roster to collect the list of persons
        2.1) get all ages of person and return that stream.
        2.2) Now calculate the average value of that stream of age that have returned.
        2.3) Now make the compiler to throw exception as NoSuchElementException if the stream is null.
        2.4) else print the average value. 
        
Pseudo code:
class AverageAge {

        public static void main(String[] args) {
            //Referring from Person.java
            List<Person> roster = Person.createRoster();
            //Get the average age and store it in average
            System.out.println("Average age : " + average);

        }
    }
*/

import java.util.List;

public class AverageAge {
    public static void main(String[] args) {
      List<Person> roster = Person.createRoster();
      
      double average = roster
          .stream()
          .mapToInt(Person::getAge)
          .average()
          .getAsDouble();
          
      System.out.println("Average age : " + average);
    }
}