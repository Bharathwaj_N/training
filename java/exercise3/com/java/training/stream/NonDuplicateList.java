package com.java.training.stream;

/*
Requirement:
    Consider a following code snippet:
        List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
    - Get the non-duplicate values from the above list using java.util.Stream API

Entity:
    NonDulicateList

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1) Create a list of Integer type and assign the given values.
    2) Create a List of integer type to store the non duplicate values.
          2.1) Find the non repeated elements using stream and Store the elements in list.
    3) For each elements in withoutDuplicate list .
          3.1)Print the elements .
     
Pseudocode:
public class NonDuplicateList {
    
    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        //filter the elements which is non repeated and store it in.
        Stream<Integer> stream = withoutDuplicate.stream();
        stream.forEach(elements -> System.out.print(elements + " "));
        }
   }
        
*/

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NonDuplicateList {
    
    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        List<Integer> withoutDuplicate = randomNumbers.stream()
                .filter(element -> Collections.frequency(randomNumbers, element) == 1)
                .collect(Collectors.toList());
        Stream<Integer> stream = withoutDuplicate.stream();
        stream.forEach(elements -> System.out.print(elements + " "));
    }
}