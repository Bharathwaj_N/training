package com.java.training.stream;

/*
Requirement:
    To filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

Entity:
    FilterPerson
    
Function Declaration:
    public static void main(String[] args)
        
Jobs To Be Done:
    1) Invoke a method createRoster from the class Person and store it to the List.
    2) Create a object for ArrayList reference with string.
    3) For each person in roster list 
    4) Check the each person's gender is equal to Male. 
           4.1) if it male store the person name in arraylist.
    5) Print the first element in arraylist as first filtered person.
    7) print last element in the arraylist as last filtered person.
    8) print the randomly filtered person.
  
Pseudo code:
  
  public class FilterPerson {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        for (Person p : roster) {
            if(gender==male){
                //add name in arraylist
             }
        }
        //get the size of array
        System.out.println("The First Filtered Person is " + name.get(0));
        System.out.println("The Last Filtered Person is " + name.get(size - 1));    
        Random random = new Random();
        //get the random index
        System.out.println("The Random Filtered Person is " + name.get(index));
        }
   }
               
*/

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FilterPerson {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		ArrayList<String> name = new ArrayList<>();
		for (Person p : roster) {
			if (p.getGender() == Person.Sex.MALE) {
				name.add(p.getName());
			}
		}
		int size = name.size();
		System.out.println("The First Filtered Person is " + name.get(0));
		System.out.println("The Last Filtered Person is " + name.get(size - 1));

		Random random = new Random();
		int index = random.nextInt(size);
		System.out.println("The Random Filtered Person is " + name.get(index));

	}
}
