package com.java.training.stream;

/*
Requirement:
    Sort the roster list based on the person's age in descending order using java.util.Stream

Entity:
    SortStream
      
Function declaration:
    public static void main(String[] args)
 
Jobs to be done:
   1)Invoke a method createRoster from the class Person and store it to the List.
   2)Sort the roaster  list in descending order using stream  based on the person age and store it on newroaster list .
   3)For each elements in newRoster list .
       3.1)Print the elements.
      
      
Pseudo code:
 public class PersonDesc {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        //sort the roster using stream.
        Stream<Person> stream = newRoster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}
*/

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortStream {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = roster.stream().sorted(Comparator.comparing(Person::getAge)).collect(Collectors.toList());
        Stream<Person> stream = newRoster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}