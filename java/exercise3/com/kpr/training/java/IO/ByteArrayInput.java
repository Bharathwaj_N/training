package com.kpr.training.java.IO;

/*
 * Requirement:
 * 		To write a program for ByteArrayInputStream class to read byte array as input stream.
 * 
 * Entity:
 * 		ByteArrayInput
 * 
 * Method Signature:
 * 		public static void main(String[] args) 
 * 
 * Jobs To be Done:
 * 		1)Create a numbers to be added to the ByteArray system array.
 * 		2)Add those numbers to the byte array input stream system.
 * 		3)It access the array as byte read and process the required operation for it.
 * 
 * Pseudo Code:
 * 		class ByteArrayInput {
 * 			public static void main(String[] args) {
 * 				byte[] buf = {"Some numbers"};
 * 
 * 				//Assign the values to the byte input stream
 * 				ByteArrayInputStream byt = new ByteArrayInputStream(buf);  
 *  
 *  			//print the each value of input value into the special characters by reference of ascii code.
 * 			}
 * 		}
 */

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ByteArrayInput {
	 
    public static void main(String[] args) throws IOException {  
		    byte[] buf = { 35, 36, 37, 38 };  
		    // Create the new byte array input stream  
		    ByteArrayInputStream byt = new ByteArrayInputStream(buf);  
		    int index = 0;  
		    
		    while ((index = byt.read()) != -1) {  
		      //Conversion of a byte into character  
		      char character = (char) index;  
		      System.out.println("ASCII value of Character is:" + index + "; Special character is: " + character);  
		    }  
		  }  
}
