package com.kpr.training.java.thread;

/*
Requirement:
    To write a program of performing two tasks by two threads that implements Runnable interface.
    
Entity:
    class ThreadDemo

Function declaration:
    public static void main(String[] args)
    public void run()
    
Jobs to be done:
    1. Under a main method declare the reference for Runnable and also declare the two task
       that is to be performed concurrently.
    2. Run the tasks and print the result.
    
Pseudo code:
class ThreadDemo implements Runnable {
    
    public void run() {
        System.out.println("My task one");
    }
    
    public static void main(String[] args) {
        Runnable runnable = new ThreadDemo();
        Thread thread1 = new Thread(Runnable) {
            public void run() {
                System.out.println("THis is my task);
            }
        }
        
        Thread thread2 = new Thread (Runnable) {
            public void run() {
                 System.out.println("My task two");
             }
       }
   }
}
*/
public class ThreadDemo implements Runnable {
    
    public void run() {
        System.out.println("My task one");
    }
    
    public static void main(String[] args) {
        Runnable runnable = new ThreadDemo();
        Thread thread1 = new Thread(runnable) {
            public void run() {
                System.out.println("This is my method");
            }
        };
        
        Thread thread2 = new Thread(runnable) {
            public void run() {
                System.out.println("My task two");
            }
        };
        
        thread1.start();
        thread2.start();
    }

}
