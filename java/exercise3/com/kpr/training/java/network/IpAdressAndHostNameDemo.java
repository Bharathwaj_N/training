package com.kpr.training.java.network;

import java.net.InetAddress;

/*
Requirement:
    To determine the IP address and host name of the local computer.
    
Entity:.
    IpAdressAndHostNameDemo
    
Method Signature:
    public static void main(String[] args)
    
Jobs to be done:
    1. An object is created for InetAddress as inetAddress that stores the address of the local host.
    2. Now print the IP address in the textual presentation.
    3. Now print the local host name for that IP address.
    
Pseudo code:
class IpAdressAnd HostNameDemo {

    public static void main(String[] args) {
        InetAddress inetAddress = InetAddress.getLocalHost();
        System.out.println("IP Address " + inetAddress.getHostAddress());
        System.out.println("Host Name " + inetAddress.getHostName());
    }
}
*/

public class IpAdressAndHostNameDemo {
    
    public static void main(String[] args) throws Exception {
        InetAddress inetAddress = InetAddress.getLocalHost();
        System.out.println("IP Address: " + inetAddress.getHostAddress());
        System.out.println("HostName: " + inetAddress.getHostName());
    }
}
