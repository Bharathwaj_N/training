package com.kpr.training.java.regex;

/*
Requirement:
    To code for java regex quantifiers
    
Entity:
    RegexQuantifiersDemo

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a String variable text and assign the value 
    2. Now use the quantifiers and print the result.
    
Pseudo code:
class RegexQuantifierDemo {
    
    public static void man(String[] args) {
    String text = "ACACCACCCAAAAACACAACACA";
    Pattern pattern = Pattern.compile("A?C);
    Matcher matcher = pattern.matcher(text);
    // print the result;
    }
}

*/
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class RegexQuantifiersDemo {
    
    public static void main(String[] args) {
        String text = "ACACCACCCAAAAACACAACACA";
        Pattern pattern = Pattern.compile("A?C");
        Matcher matcher = pattern.matcher(text);
        
        int i = 0;
        while (matcher.find()) {
            i++;
            System.out.println(i + matcher.group());
        }
        
    }
}
