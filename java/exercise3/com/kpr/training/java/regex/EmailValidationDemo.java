package com.kpr.training.java.regex;

/*
Requirement:
    To code for the validation of given mail ids.

Entity:
    EmailValidationDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a list of Strings that contains the list of mail ids of the persons.
    2. Check for the valid mail id of the person.
        2.1) Based upon the result print the result in boolean value.
        
Pseudo code:
class EmailValidationDemo {
    
    public static void main(String[] args) {
        List<String> emailId = new ArrayList<>();
        // add the emailId
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        
        for (String email : emailId) {
            Matcher matcher = pattern.matcher(email);
            // print true or false based on the emails.
       }
   }
}
 
*/

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidationDemo {
    
    public static void main(String[] args) {
        List<String> emailId = new ArrayList<>();
        emailId.add("bharathwajreach@gmail.com");
        emailId.add("bharathwaj549@gmail.com");
        emailId.add("18EE014@kpriet.ac.in");
        emailId.add("@gmail.com");
        
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        
        for (String email : emailId) {
            Matcher matcher = pattern.matcher(email);
            System.out.println(email + " : " + matcher.matches());
        }
        
    }

}
